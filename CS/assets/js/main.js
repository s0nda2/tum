/**
 * Soni D.
 * main.js
 */

var addEventListenerArbeitsblaetter = function () {

  var g = { pageNaviId      : "page_navi",
            pageContentId   : "page_content",
            linksSelector   : 'ul:nth-of-type(3) > li > a, ul:last-of-type a',
            startseiteSel   : 'div.startseite',
            blattSelector   : 'div.arbeitsblatt',
            classNameAbsend : 'btnAbsenden',
            pageNavi        : null,
            pageContent     : null,
            startseite      : null,
            arbeitslinks    : [],
            arbeitsblaetter : []
          }

  function init() {
    g.pageNavi = document.getElementById(g.pageNaviId) ? document.getElementById(g.pageNaviId) : null;
    g.pageContent = document.getElementById(g.pageContentId) ? document.getElementById(g.pageContentId) : null;
  
    if ( g.pageNavi ) {
      g.arbeitslinks = g.pageNavi.querySelectorAll(g.linksSelector) ? g.pageNavi.querySelectorAll(g.linksSelector) : [];
    }

    if ( g.pageContent ) {
      g.startseite = g.pageContent.querySelector(g.startseiteSel) ? g.pageContent.querySelector(g.startseiteSel) : null;
      g.arbeitsblaetter = g.pageContent.querySelectorAll(g.blattSelector) ? g.pageContent.querySelectorAll(g.blattSelector) : [];
    }

    return ( g.pageNavi && g.arbeitsblaetter && g.arbeitsblaetter.length > 0 ) ? true : false; 
  } // init


  function absenden( btnObj) { // btnObj : current button that is clicked on
    // Retrieve the ancestor div tag, that contains this btnObj
    var x = btnObj.parentNode;
    while ( x.getAttribute("class") !== g.classNameAbsend ) { x = x.parentNode; }
    // Traverse the questions and notice the right one(s).
    
  } // absenden
  
  
  function openBlatt ( k ) {
    g.startseite.style.visibility = "hidden";
    // Get arbeitsblatt content through Ajax.
    makeRequest("content/arbeitsblatt-" + (k+1) + ".html", g.arbeitsblaetter[k]);
    // Set the concerned arbeitsblatt visible.
    g.arbeitsblaetter[k].style.visibility = "visible";
    for ( var j = 0; j < g.arbeitsblaetter.length; j++ ) {
      if ( j != k )  g.arbeitsblaetter[j].style.visibility = "hidden";
    }
  } // openBlatt


  if ( init() ) {
    // Display Startseite
    makeRequest("content/startseite.html", g.startseite);
    // Arbeitslinks
    for ( var i = 0; i < g.arbeitslinks.length; i++ ) {
      g.arbeitslinks[i].addEventListener("click", function ( evt ) {
        for ( var j = 0; j < g.arbeitslinks.length; j++ ) {
          if ( evt.target === g.arbeitslinks[j] ) {
            openBlatt(j);
            break;
          }
        }
      }, false);
    }
    // console.log("g.arbeitsblaetter.length : " + g.arbeitsblaetter.length);
  }

} // addEventListenerArbeitsblaetter

window.onload = addEventListenerArbeitsblaetter;
