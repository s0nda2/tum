/**
 * 
 * Soni D.
 * makeRequest
 *
 * Reference: https://developer.mozilla.org/en-US/docs/AJAX/Getting_Started
 */
 
  var http_request = false;
  var seite = null;
  
  function makeRequest ( url, obj ) { // obj : Object that is calling this method
    if ( window.XMLHttpRequest ) { // Mozilla, Safari, IE7+ etc.
      http_request = new XMLHttpRequest();
      /* Verwende .overrideMimeType() um den Server zu erzwingen, einen Stream
       * als 'text/xml' zu behandeln und parsen, selbst wenn ihn der Server nicht 
       * als das meldet.
       * .overrideMimeType() muss vor .send() aufgerufen werden.
       */
      //if ( http_request.overrideMimeType ) {
        //http_request.overrideMimeType('text/xml');
      //}
    } else if ( window.ActiveXObject ) { // IE6 and older
        try {
          http_request = new ActiveXObject("Msxml2.XMLHTTP");
        } catch ( e ) {
          try {
            http_request = new ActiveXObject("Microsoft.XMLHTTP");
          } catch ( e ) {}
        }
    }
    
    if ( !http_request || obj == null ) { return false; }    
    seite = obj;
    
    // Handles processing the server's response
    http_request.onreadystatechange = displayArbeitsblatt;
        
    // Using HTTP methods to make the request
    http_request.open('GET', url, true); // true : asynchronous
    
    // Sends the request to server
    http_request.send(null);
  } // makeRequest
  
  
  function displayArbeitsblatt () {
    if ( http_request.readyState === 4 ) { // XMLHttpRequest.DONE
     if ( http_request.status === 200 ) {
       var content = http_request.responseText; // .responseXML;
       seite.innerHTML = content;
       formatCode( seite );
       /* Calls MathJax preprocessors to run over the page again,
        * i.e. for calls using ajax (XMLHttpRequest).
        * http://docs.mathjax.org/en/latest/typeset.html#modifying-math-on-the-page
        */
       MathJax.Hub.Queue(["Typeset", MathJax.Hub, seite]);
     }
    }
  } // displayArbeitsblatt


  function formatCode( arbeitsblatt ) {
    var selector = 'div.arbeitsblatt_code';
    var divs = arbeitsblatt.querySelectorAll(selector) ? arbeitsblatt.querySelectorAll(selector) : [];
    if ( !divs || divs.length == 0 )  return;
    var separator = /\s*\n\s*/g;
    for ( var k = 0; k < divs.length; ++k ) {
      var div = divs[k];
      if ( !div )  return; // check empty string 
      var lines = div.innerHTML.split(separator);
      if ( !lines.length )  return;

      var tabl = document.createElement("table");
      var tbody = document.createElement("tbody");
      var vTitle = div.getAttribute("title");

      var i = 0;
      var j = ( vTitle !== null ) ? parseInt(vTitle.substring(1)) : 1;

      do {
        if ( lines[i].trim() == "" ) { ++i; continue; }
        var row = document.createElement("tr");
        // 1st column : line number
        var col = document.createElement("td");
        col.appendChild(document.createTextNode("" + (j++)));
        col.style.textAlign = "right";
        col.style.color = "#004682";
        row.appendChild(col);
        // 2nd column : content/code
        col = document.createElement("td");
        lines[i] = lines[i].replace(/_/g, '\u00A0'); // 'u00A0' is white space 
        lines[i] = lines[i].replace(/&lt;/g, '\u003C');
        lines[i] = lines[i].replace(/&gt;/g, '\u003E');
        lines[i] = lines[i].replace(/<=/g, '\u2264');
        lines[i] = lines[i].replace(/>=/g, '\u2265');
        col.appendChild(document.createTextNode('\u00A0\u00A0'));
        col.appendChild(document.createTextNode(lines[i++]));
        row.appendChild(col);
        // Add row to table body
        tbody.appendChild(row);
      } while ( i < lines.length );
      tabl.appendChild(tbody);
      div.innerHTML = "";
      div.appendChild(tabl);
    }
  } // formatCode

