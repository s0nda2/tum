/*********************************************************************
 *
 *  Aufgabe 402d
 *
 *  Implementieren Sie nun die Funktion dez2bin. Kopieren Sie dazu
 *  Ihren Quellcode aus Aufgabe 206 und entfernen Sie alle Ein- und
 *  Ausgaben. Füllen Sie stattdessen schrittweise die Zeichenkette
 *  binaerString. Schreiben Sie keine eigene Funktion main! Wir haben
 *  diese bereits selbst geschrieben. Unsere Funktion übernimmt auch
 *  alle nötigen Ein- und Ausgaben.
 *
 *********************************************************************/

/* 
 * dezimal:      die zu konvertierende Zahl
 * binaerString: die binäre Darstellung von dezimal
 *               Länge 32: 31 bit + '\0' 
 */

void dez2bin(int dezimal, char binaerString[32])
{

}

