/*********************************************************************
 *
 *  Aufgabe 404g
 *
 *  Das gegebene Programm kann nur mit Matrizen der festen Größe 3x3
 *  arbeiten. Modifizieren Sie den Code deshalb so, dass er zuerst vom
 *  Benutzer die gewünschte Matrizengröße mit dem Text
 *
 *    "Groesse der quadratischen Matrix: "
 *
 *  abfragt. Anschließend soll Ihr Programm ein Feld mit der
 *  gewünschten Größe erzeugen und die Elemente entsprechend
 *  einlesen. Zum Schluss soll Ihr Programm wieder wie vorgegeben die
 *  transponierte Matrix ausgegeben.
 *
 *********************************************************************/


#include <stdio.h>
#include <stdlib.h>


int main()
{
	int z=0, s=0;
	double *matrix;

	/* Speicher reservieren */
	matrix = (double *) malloc(3*3 * sizeof(double));
	
	/* Matrixelemente einlesen */
	for(z = 0; z < 3; ++z)
	{
		for (s = 0; s < 3; ++s)
		{
			printf("Element %i/%i: ", z, s);
			scanf("%lf", &matrix[ z*3 + s ]);
		}
	}

	/* Ausgeben der Transponierten */
	printf("Die Transponierte lautet:\n");
	
	/* Beim Transponieren werden Zeilen und Spalten vertauscht: */
	for(s = 0; s < 3; ++s)
	{
		for(z = 0; z < 3; ++z)
		{
			printf("%8.2lf", matrix[ z*3 + s ]);
		}

		printf("\n");
	}

	return 0;
}

