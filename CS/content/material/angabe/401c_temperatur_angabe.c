/*********************************************************************
 *
 *  Aufgabe 401c
 *
 *  Schreiben Sie unten stehendes Programm so um, dass in den
 *  Unterfunktionen nur noch die Umrechnungsformeln verbleiben, alle
 *  printf- und scanf-Aufrufe dagegen in main zu finden sind. Die
 *  Funktionalität soll dabei exakt erhalten bleiben. Zwischen main
 *  und den Unterfunktionen sollen Sie die nötigen Informationen durch
 *  Funktionsparameter und Rückgabewerte austauschen.
 *
 *********************************************************************/


#include <stdio.h>


void fahrenheitToCelsius()
{
	double fahrenheit, celsius;

	printf("Temperatur in Fahrenheit: ");
	scanf("%lf", &fahrenheit);

	celsius = 5.0/9.0 * (fahrenheit - 32.0);
	printf("Temperatur in Grad Celsius: %.1lf\n", celsius);
}


void celsiusToFahrenheit()
{
	double celsius, fahrenheit;

	printf("Temperatur in Grad Celsius: ");
	scanf("%lf", &celsius);

	fahrenheit = 9.0/5.0 * celsius + 32.0;
	printf("Temperatur in Fahrenheit: %.1lf\n", fahrenheit);
}


int main()
{
	int menue;

	do 
	{
		/* Menü */
		printf("\n" );
		printf("*****************************************\n");
		printf("* Bitte waehlen Sie eine Umrechnungsart:\n");
		printf("* 1) Grad Celsius in Fahrenheit umrechnen\n");
		printf("* 2) Fahrenheit in Grad Celsius umrechnen\n");
		printf("* 3) Beenden\n");
		printf("*****************************************\n");
		printf("Auswahl: ");
		scanf("%i", &menue);

		/* Temperatur umrechnen */
		switch(menue) 
		{
			case 1:
				celsiusToFahrenheit();
				break;
			
			case 2:
				fahrenheitToCelsius();
				break;
		}

	} while (menue != 3);

	return 0;
}

