/*********************************************************************
 *
 *  Aufgabe 408a
 *
 *  Schreiben Sie den Quicksort-Algorithmus aus Aufgabe 403 so um,
 *  dass er mit Zeigern arbeitet statt mit Feldern. Er ist dadurch
 *  schneller, übersichtlicher und kommt ohne globale Variablen
 *  aus. Implementieren Sie dazu die Funktion Quicksort.
 *
 *  Die main-Funktion ist bereits vorgegeben. Sie kümmert sich um das
 *  Einlesen und Ausgeben der Zahlen. Lassen Sie diese Funktion bitte
 *  unbedingt unverändert. Implementieren Sie lediglich eine Funktion
 *  Quicksort gemäß obigen Vorgaben.
 *
 *********************************************************************/


#include <stdio.h>
#include <stdlib.h>


void Quicksort(int *links, int *rechts);


int main()
{
	int *i, *Feld;

	Feld = (int*) malloc(sizeof(int) * 100);
	
	/* Einlesen der Liste */
	i = Feld;
	do
	{
		printf("Bitte geben Sie eine Zahl ein oder Null zum Abbruch: ");
		scanf("%i", i);

		if (*i == 0)  break;

		++i;

	} while (i-Feld < 100);
	
	/* Sortieren der Liste */
	Quicksort(Feld, i-1);

	/* Ausgeben der sortierten Liste */
	while(Feld != i)
	{
		printf("%i  ", *(Feld++));
	}
	
	printf("\n");

	return 0;
}

