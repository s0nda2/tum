
/*********************************************************************
 *
 *  Aufgabe 405c
 *
 *  Schreiben Sie nun einen dynamisch rekursiven Algorithmus zur
 *  Berechnung der n-ten Q-Zahl, indem Sie die beiden Funktionen
 *
 *    void dynInit(int n, int cache[])
 *    int qRekursivDyn(int n, int cache[])
 *
 *  implementieren. Dynamische Programmierung zeichnet sich wesentlich
 *  dadurch aus, dass der Programmierer einen Cache verwendet, um
 *  wichtige Zwischenergebnisse speichern und später wiederverwenden
 *  zu können.
 *
 *  Die main-Funktion ist wieder in einer anderen Datei von uns
 *  vorgegeben. Dort wird auch ein Feld der Größe n+1 zum Cachen der
 *  Q-Zahlen erstellt. In diesem Feld sollen Sie beim Index n die
 *  jeweils n-te Q-Zahl speichern. main ruft anfangs einmal dynInit
 *  auf; in dieser Funktionen führen Sie gegebenenfalls notwendige
 *  Initialisierungen durch. Nach der Funktion dynInit ruft main die
 *  Funktion qRekursivDyn dreimal auf, jeweils mit verschiedenen
 *  Werten für n. Zwischen den Aufrufen gibt main jeweils den Inhalt
 *  des Caches (d.h. den Inhalt des Felds) aus.
 *
 *   - Implementieren Sie die Funktion dynInit. Der Parameter n
 *     bezeichnet die größt mögliche Q-Zahl, welche im aktuellen
 *     Programmdurchlauf berechnet werden wird. Der Cache-Speicher
 *     wurde bereits reserviert; Sie sollen dessen Elemente 1 und 2
 *     mit Eins und den Rest mit Null initialisieren. (Bei dieser
 *     Zählweise wird der Speicher cache[0] nicht verwendet.) Beachten
 *     Sie, dass n jede beliebige ganze Zahl größer Null sein kann.
 *
 *   - Die Funktion qRekursivDyn soll mit rekursiven Aufrufen Q-Zahlen
 *     berechnen und gleichzeitig alle (Zwischen-)Ergebnisse in einem
 *     Zwischenspeicher aufbewahren. Hat qRekursivDyn die gewünschte
 *     Q-Zahl bereits gespeichert, so soll sie diese direkt aus dem
 *     Speicher holen – ohne weitere Berechnungen.
 *
 *********************************************************************/


#include <stdio.h>


void dynInit(int n, int cache[])
{

}


int qRekursivDyn(int n, int cache[])
{

}

