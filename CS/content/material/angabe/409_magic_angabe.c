/*********************************************************************
 *
 *  Aufgabe 501
 *
 *  Eine quadratische Anordnung von n Zahlen heißt magisches Quadrat
 *  der Ordnung n, wenn das Quadrat die Zahlen 1 bis n*n so enthält,
 *  dass alle Summen jeweils einer Zeile, einer Spalte bzw. einer der
 *  beiden Diagonalen denselben Wert - die magische Zahl - haben. Die
 *  Ordnung des folgenden Quadrats ist 5, die magische Zahl ist 65.
 *
 *   17  24   1   8  15
 *   23   5   7  14  16
 *    4   6  13  20  22
 *   10  12  19  21   3
 *   11  18  25   2   9
 *
 *
 *  Ein Verfahren zur Bestimmung magischer Quadrate ungerader Ordnung
 *  stammt von De La Loubere, der es 1687 aus China mit brachte. Die
 *  Zahlen 1 bis n·n werden in aufsteigender Reihenfolge auf die
 *  folgenden Einträge (Orte im Quadrat) verteilt: Ausgehend vom
 *  mittleren Eintrag der ersten Zeile, der mit der Zahl 1 belegt wird,
 *  geht man in Einzelschritten jeweils einen Eintrag schräg nach rechts
 *  oben. Gelangt man an den oberen Rand, so geht man in die unterste
 *  Zeile; entsprechend springt man in die erste Spalte, wenn man den
 *  rechten Rand des Quadrats überschreitet. Falls man im nächsten 
 *  Schritt auf einen schon besetzten Eintrag kommen würde, oder man 
 *  sich bereits im Eintrag ganz rechts oben befindet, liegt der nächste
 *  Eintrag unmittelbar senkrecht unter dem aktuellen.
 *
 *
 *  Implementieren Sie ein magisches Quadrat, wie es im Arbeitsblatt
 *  beschrieben ist. Fragen Sie hierzu zu Beginn des Programms die
 *  Ordnung n mit folgendem Text ab:
 *
 *    "Ordnung des magischen Quadrats: "
 *
 *  Überprüfen Sie dabei, ob der eingegebene Wert ungerade und kleiner
 *  20 ist. Sollte dies nicht zutreffen, beenden Sie das Programm ohne
 *  eine weitere Ausgabe. Ist die Eingabe für die Ordnung passend,
 *  geben Sie das magische Quadrat aus, wobei jede Zahl mit einer
 *  Breite von fünf Stellen rechtsbündig ausgegeben werden
 *  soll. Vergleichen Sie bitte das mit dem Verhalten der
 *  Referenzlösung!
 *
 *********************************************************************/



#include <stdio.h>


int main()
{

}
