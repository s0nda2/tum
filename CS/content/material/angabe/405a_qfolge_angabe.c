/*********************************************************************
 *
 *  Aufgabe 405a
 *
 *  Verfassen Sie nun einen Algorithmus, der die n-te Q-Zahl in
 *  iterativer Form berechnet, indem Sie die Funktion
 *
 *    int qIterativ(int n)
 *
 *  implementieren. Der Parameter n gibt dabei die Nummer der Q-Zahl
 *  an, die Ihre Funktion berechnen und als Rückgabewert an main
 *  zurückgeben soll. Die aufrufende Funktion (main) stellt sicher,
 *  dass n eine beliebige ganze Zahl größer als Null ist.
 *
 *  Die main-Funktion ist bereits vorgegeben. Sie befindet sich in
 *  einer anderen Datei und übernimmt alle Ein- und Ausgaben. Sie
 *  müssen also nur noch die Berechnung implementieren.
 *
 *********************************************************************/


#include <stdio.h>
#include <stdlib.h>


int qIterativ(int n)
{

}

