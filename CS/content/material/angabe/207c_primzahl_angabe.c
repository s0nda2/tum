/*********************************************************************
 *
 *  Aufgabe 207c
 *
 *  Eine Primzahl ist eine natürliche Zahl größer 1, die nur durch
 *  sich selbst und 1 teilbar ist. In dieser Aufgabe entwickeln Sie
 *  einen einfachen Algorithmus, mit dem sich alle Primzahlen bis zu
 *  einem vom Benutzer vorgegebenen Obergrenze (Maximum) berechnen
 *  lassen.
 *
 *  Lesen Sie also zuerst die Obergrenze vom Benutzer ein. Verwenden
 *  Sie dazu die Aufforderung
 *
 *    "Bitte geben Sie die Obergrenze ein: "
 *
 *  Die Eingabe ist größer 1; Sie brauchen das nicht zu
 *  prüfen. Berechnen Sie als nächstes schrittweise bei 2 beginnend
 *  alle Primzahlen bis einschließlich der Obergrenze. Geben Sie diese
 *  dann der Reihe nach mit dem Formatstring "%5i" aus. Schließen Sie
 *  Ihre Ausgabe am Ende mit einem Zeilenumbruch ab.
 *
 *********************************************************************/
 

#include <stdio.h>
 

int main()
{
}
