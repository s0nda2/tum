/*********************************************************************
 *
 *  Aufgabe 206h
 *
 *  Ihr Programm dieser Aufgabe soll das Bitmuster einer ganzen Zahl
 *  (int) ausgeben. Hierfür wenden Sie Bitoperatoren (>> und &) an.
 *
 *  Auf dem Arbeitsblatt 2 im Internet finden Sie den Algorithmus in
 *  verbalisierter Form. Setzen Sie nun den Algorithmus in der Sprache
 *  C um.
 *
 *********************************************************************/


#include <stdio.h>


int main()
{
	int dezimal, aktBit;

	printf("Geben Sie bitte eine Dezimalzahl ein: ");
	scanf("%i", &dezimal);

	if ( dezimal < 0 ) {
		printf("Die Zahl muss groesser gleich Null sein!");
		printf("\n");
		return -1;
	}

	printf("Die zugehoerige Binaerzahl lautet: ");

	/* Algorithmus */
	for ( int i = 30; i >= 0; --i ) {
		aktBit = (dezimal >> i) & 0x00000001;
		printf("%i", aktBit);
	}

	return 0;
}

