/*********************************************************************
 *
 *  Aufgabe 307c
 *
 *  Schreiben Sie ein Programm, um den Wochentag an einem bestimmten
 *  Datum zu bestimmen. Berücksichtigen Sie folgende Regeln für das
 *  Schaltjahr. Sie haben diese Bedingungen programmiertechnisch
 *  bereits in Aufgabe 204 umgesetzt.
 *
 *    - Ein Jahr, dessen Jahreszahl durch 4 teilbar ist, ist ein
 *      Schaltjahr ...
 *
 *    - ..., außer wenn die Jahreszahl durch 100 teilbar ist.
 *
 *    - Ist die Jahreszahl durch 400 teilbar, dann ist das
 *      entsprechende Jahr aber trotzdem ein Schaltjahr.
 *      (Beispielsweise ist 1900 kein Schaltjahr, 2000 ist aber ein
 *      Schaltjahr.)
 *
 *  Zusätzlich soll das (fiktive) Jahr 0 kein Schaltjahr und der
 *  1. Januar 00 ein Sonntag gewesen sein.
 *
 *  Fragen Sie zu Beginn das Datum mit
 *
 *    "Jahr: "
 *    "Monat: "
 *    "Tag: "
 *
 *  ab. Überprüfen Sie dessen Korrektheit (Tag und Monat im gültigen
 *  Bereich; Jahr ≥ 0) und brechen Sie bei einer fehlerhaften Eingabe
 *  sofort mit der Meldung
 *
 *   "Falsche Eingabe."
 *
 *  ab. War das eingegebene Datum in Ordnung, geben Sie den Wochentag
 *  mit
 *
 *    "Der xx.xx.xxxx ist ein yyyyyyy."
 *
 *  aus. Dabei ist xx.xx.xxxx das eingegebene Datum, beispielsweise in
 *  der Form 13.01.736, und yyyyyyy der Wochentag, beispielsweise
 *  Donnerstag.
 *
 *  Beachten Sie bitte auch die zusätzlichen Hilfestellungen auf dem
 *  Arbeitsblatt.
 *
 *********************************************************************/

#include <stdio.h>


/**
 * Function declarations
 */
int istSchaltjahr(int);
int isValidDay(int, int, int, const unsigned short *);


/**
 * 0 : FALSE
 * 1 : TRUE (Schaltjahr)
 */
int istSchaltjahr(int jahr) {
  return ( jahr <= 0 ) ? 0 : ( jahr % 400 == 0 ) ? 1 : ( jahr % 100 == 0 ) ? 0 : ( jahr % 4 == 0 ) ? 1 : 0;
}


/**
 * 0 : FALSE
 * 1 : TRUE (Valid day)
 */
int isValidDay(int tag, int monat, int jahr, const unsigned short *monatstage) {
  return ( tag < 1 || tag > monatstage[monat-1] ) ? ( istSchaltjahr(jahr) && monat == 2 && tag == 29 ) ? 1 : 0 : 1;
}



/**
 *
 */
int main() {
  const unsigned short monatstage[12] = {
    // 1., 2., 3., 4., 5., 6., 7., 8., 9., 10., 11., 12.
       31, 28, 31, 30, 31, 30, 31, 31, 30, 31,  30,  31
  };

  const char *wochentage[] = {
    "Sonntag", "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag"
  };

  int jahr;
  unsigned short monat, tag;

  printf("Jahr: ");
  scanf("%i", &jahr);
  if ( jahr < 0 ) {
    printf("Falsche Eingabe.");
    return -1;
  }

  printf("Monat: ");
  scanf("%hu", &monat);
  if ( monat < 1 || monat > 12 ) {
    printf("Falsche Eingabe.");
    return -1;
  }

  printf("Tag: ");
  scanf("%hu", &tag);
  if ( !isValidDay(tag,monat,jahr,monatstage) ) { // ungueltiger Tag
    printf("Falsche Eingabe.");
    return -1;
  }

  /*
   * Die Zeitrechnung beginnt mit dem (fiktiven) Jahr 0, das kein Schaltjahr ist.
   * Der erste Tag ist der "01. Januar 00". Zudem ist dieser Tag ein Sonntag gewesen.
   */
  int vTage = 0;
  for ( int j = 0; j < jahr; j++ ) { vTage += ( istSchaltjahr(j) ) ? 366 : 365; }
  for ( int m = 1; m < monat; m++ ) { vTage += ( m == 2 && istSchaltjahr(jahr) ) ? 29 : monatstage[m-1]; }
  vTage += (tag-1);
  vTage %= 7;

  printf("Der %02hu.%02hu.%i ist ein %s.\n", tag, monat, jahr, wochentage[vTage]);

  return 0;
}

