/*********************************************************************
 *
 *  Aufgabe 307c
 *
 *  Schreiben Sie ein Programm, um den Wochentag an einem bestimmten
 *  Datum zu bestimmen. Berücksichtigen Sie folgende Regeln für das
 *  Schaltjahr. Sie haben diese Bedingungen programmiertechnisch
 *  bereits in Aufgabe 204 umgesetzt.
 *
 *    - Ein Jahr, dessen Jahreszahl durch 4 teilbar ist, ist ein
 *      Schaltjahr ...
 *
 *    - ..., außer wenn die Jahreszahl durch 100 teilbar ist.
 *
 *    - Ist die Jahreszahl durch 400 teilbar, dann ist das
 *      entsprechende Jahr aber trotzdem ein Schaltjahr.
 *      (Beispielsweise ist 1900 kein Schaltjahr, 2000 ist aber ein
 *      Schaltjahr.)
 *
 *  Zusätzlich soll das (fiktive) Jahr 0 kein Schaltjahr und der
 *  1. Januar 00 ein Sonntag gewesen sein.
 *
 *  Fragen Sie zu Beginn das Datum mit
 *
 *    "Jahr: "
 *    "Monat: "
 *    "Tag: "
 *
 *  ab. Überprüfen Sie dessen Korrektheit (Tag und Monat im gültigen
 *  Bereich; Jahr ≥ 0) und brechen Sie bei einer fehlerhaften Eingabe
 *  sofort mit der Meldung
 *
 *   "Falsche Eingabe."
 *
 *  ab. War das eingegebene Datum in Ordnung, geben Sie den Wochentag
 *  mit
 *
 *    "Der xx.xx.xxxx ist ein yyyyyyy."
 *
 *  aus. Dabei ist xx.xx.xxxx das eingegebene Datum, beispielsweise in
 *  der Form 13.01.736, und yyyyyyy der Wochentag, beispielsweise
 *  Donnerstag.
 *
 *  Beachten Sie bitte auch die zusätzlichen Hilfestellungen auf dem
 *  Arbeitsblatt.
 *
 *********************************************************************/


#include <stdio.h>

/**
 * Checks if the given year number (jahr) is a leap year (Schaltjahr).
 * If jahr is a leap year, then returns 1. Otherwise 0.
 *
 * 0 = Kein Schaltjahr (FALSE)
 * 1 = Schaltjahr      (TRUE)
 */
int checkSchaltjahr(int jahr) {
  return ( jahr == 0 ) ? 0 : ( ( jahr % 100 == 0 ) ? ( ( jahr % 400 != 0 ) ? 0 : 1 ) : ( ( jahr % 4 != 0 ) ? 0 : 1 ) );
}

/**
 * HAUPTPROGRAMM
 */
int main()
{
  char* wochentag[] = {
     "Sonntag", "Montag", "Dienstag", "Mittwoch",
     "Donnerstag", "Freitag", "Samstag"
  };

  unsigned short monatstage[] = {
  // Jan, Feb, Mrz, Apr, Mai, Jun, Jul, Aug, Sep, Okt, Nov, Dez
      31,  28,  31,  30,  31,  30,  31,  31,  30,  31,  30,  31
  };

  int jahr; // 0..inf
  unsigned short monat; // 1..12
  unsigned short tag; // 1..28,29,30,31

  int istSchaltjahr = 0; /* 0 = kein Schaltjar (false),
                          * 1 = Schaltjar (true)
                          */
  int vTage = 0; // Anzahl der Tage, die seit dem 1.1.00 vergangen sind.

  /*
   * Abfragen des Datums.
   */
  printf("Jahr: ");
  scanf("%i", &jahr);
  if ( jahr < 0 ) {
    printf("Falsche Eingabe.");
    return -1; // Error. Exit Program!
  }

  printf("Monat: ");
  scanf("%hu", &monat);
  if ( !(monat >= 1 && monat <= 12) ) {
    printf("Falsche Eingabe.");
    return -1; // Error. Exit Program!
  }

  printf("Tag: ");
  scanf("%hu", &tag);
  if ( !(tag >=1 && tag <= 31) ) {
    printf("Falsche Eingabe.");
    return -1; // Error. Exit Program!
  }

  /*
   * Zusaetzliche Ueberpruefungen
   */
  istSchaltjahr = checkSchaltjahr(jahr);
  if ( ((monat == 1 || monat == 3 || monat == 5 || monat == 7 || monat == 8 || monat == 10 || monat == 12) && tag > 31)
       || ((monat == 2 || monat == 4 || monat == 6 || monat == 9 || monat == 11) && tag > 30)
       || ( istSchaltjahr == 1 && monat == 2 && tag > 29 )
       || ( istSchaltjahr == 0 && monat == 2 && tag > 28 ) ) {
    printf("Falsche Eingabe.");
    return -1; // Error. Exit Program!
  }

  /*
   * Berechne die Anzahl der Tage, die seit dem 1.1.00 vergangen sind.
   */
  // Schritt 1: Berechne Anzahl der vergangenen Tage vom Jahr 00 bis zum Jahr <jahr-1>.
  //            Das jahr <jahr> wird später gesondert behandelt.
  for ( int j = 0; j < jahr; j++ ) {
    istSchaltjahr = checkSchaltjahr(j);
    vTage += ( istSchaltjahr == 0 ) ? 365 : 366;
  }

  // Schritt 2: Berechne Anzahl der vergangenen Tage vom Monat 1 bis Monat <monat-1>
  //            in dem Jahr <jahr>. Dabei wird beachtet, dass Februar in Schaltjahren 29,
  //            sonst nur 28 Tage hat. Der Monat <monat> wird später gesondert behandelt.
  istSchaltjahr = checkSchaltjahr(jahr);
  for ( short m = 1; m < monat; m++ ) {
    vTage += monatstage[m-1];
    if ( istSchaltjahr == 1 && m == 2 ) { vTage++; }
  }

  // Schritt 3: Behandle den Monat <monat>. Berechne die vergangenen Tage in diesem
  //            Monat bis zum vorgegebenen Datum.
  for ( int t = 1; t < tag; t++ )
    vTage++;

  /*
   * Berechne den entsprechenden Wochentag fuer das vorgegebene Datum.
   */
  vTage %= 7;

  printf("Der %02hu.%02hu.%i ist ein %s.\n", tag, monat, jahr, wochentag[vTage]);
  return 0;
}

