/*********************************************************************
 *
 *  Aufgabe 402b
 *
 *  Dieser Quellcode ist bereits fertig. Er dient Ihnen für eigene
 *  Experimente. Lesen Sie dazu auch die Angabe zu Aufgabe 402 auf dem
 *  vierten Arbeitsblatt.
 *
 *********************************************************************/


#include <stdio.h>


void f(int n, int ifeld[])
{
	int i=0;

	n = 5;
	
	for (i=0; i<n; ++i) 
	{
		ifeld[i] = i;
	}

	printf("f:n: %i, ", n); // f:n: 5
	printf("f:ifeld:");
	
	for (i=0; i<n; ++i) 
	{
		printf(" %i", ifeld[i]);
	} // f:ifeld: 0 1 2 3 4
	printf(". ");
}


int main()
{
	int i=0, n=10;
	int ifeld[10] = {0}; // 0 0 0 0 0 0 0 0 0 0

	f(n, ifeld);
	
	printf("main:n: %i, ", n); // main:n: 10
	printf("main:ifeld:");
	
	for(i = 0; i < n; ++i) 
	{
		printf(" %i", ifeld[i]);
	} // main:ifeld: 0 1 2 3 4 0 0 0 0 0 0
	printf("\n");
}

