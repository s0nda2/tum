/*********************************************************************
 *
 *  Aufgabe 404g
 *
 *  Das gegebene Programm kann nur mit Matrizen der festen Größe 3x3
 *  arbeiten. Modifizieren Sie den Code deshalb so, dass er zuerst vom
 *  Benutzer die gewünschte Matrizengröße mit dem Text
 *
 *    "Groesse der quadratischen Matrix: "
 *
 *  abfragt. Anschließend soll Ihr Programm ein Feld mit der
 *  gewünschten Größe erzeugen und die Elemente entsprechend
 *  einlesen. Zum Schluss soll Ihr Programm wieder wie vorgegeben die
 *  transponierte Matrix ausgegeben.
 *
 *********************************************************************/


#include <stdio.h>
#include <stdlib.h>


int main()
{
	int z=0, s=0, groesse;
	double *matrix;

	/* Matrixgroesse einlesen */
	printf("Groesse der quadratischen Matrix: ");
	scanf("%i", &groesse);

	/* Speicher reservieren */
	matrix = (double *) malloc(groesse * groesse * sizeof(double));
	
	/* Matrixelemente einlesen */
	for(z = 0; z < groesse; ++z)
	{
		for (s = 0; s < groesse; ++s)
		{
			printf("Element %i/%i: ", z, s);
			scanf("%lf", &matrix[ z * groesse + s ]);
		}
	}

	/* Ausgeben der Transponierten */
	printf("Die Transponierte lautet:\n");
	
	/* Beim Transponieren werden Zeilen und Spalten vertauscht: */
	for(s = 0; s < groesse; ++s)
	{
		for(z = 0; z < groesse; ++z)
		{
			printf("%8.2lf", matrix[ z * groesse + s ]);
		}

		printf("\n");
	}

	return 0;
}

