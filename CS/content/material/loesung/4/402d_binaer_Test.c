/*********************************************************************
 *
 *  Aufgabe 402d
 *
 *  Implementieren Sie nun die Funktion dez2bin. Kopieren Sie dazu
 *  Ihren Quellcode aus Aufgabe 206 und entfernen Sie alle Ein- und
 *  Ausgaben. Füllen Sie stattdessen schrittweise die Zeichenkette
 *  binaerString. Schreiben Sie keine eigene Funktion main! Wir haben
 *  diese bereits selbst geschrieben. Unsere Funktion übernimmt auch
 *  alle nötigen Ein- und Ausgaben.
 *
 *********************************************************************/

/*
 * dezimal:      die zu konvertierende Zahl
 * binaerString: die binäre Darstellung von dezimal
 *               Länge 32: 31 bit + '\0'
 */

void dez2bin(int dezimal, char binaerString[32])
{
	for ( int i = 30; i >= 0; --i ) {
		binaerString[30-i] = '0' + ((dezimal >> i) & 0x00000001);
		// Or: Add 48 to convert value 0 (or 1) to ASCII '0' (or '1')
		// binaerString[30-i] = 48 + ((dezimal >> i) & 0x00000001);
	}
}


/*
 * main Funktion wird nur zum Testzweck implementiert.
 */
int main() {
	int dezimal;
	char binaerString[32]; // 0..31
	binaerString[31] = '\0';

	printf("Geben Sie bitte eine Dezimalzahl ein: ");
	scanf("%i", &dezimal);

	dez2bin(dezimal, binaerString);

	printf("Die zugehoerige Binaerzahl lautet: ");

	/*
	for ( int i = 0; i <= 30; ++i ) {
		printf("%c", binaerString[i]);
	}*/
	printf("%s", binaerString);
	printf("\n");
}

