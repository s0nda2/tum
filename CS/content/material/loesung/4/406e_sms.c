/*********************************************************************
 *
 *  Aufgabe 406e
 *
 *  Implementieren Sie eine Warteschlange in C als einfach verkettete
 *  Liste, indem Sie die vorgegebenen Funktionen
 *
 *    void push(struct Queue *queue, struct SMS *new) und
 *    struct SMS *pop(struct Queue *queue)
 *
 *  füllen. Die main-Funktion ist bereits an anderer Stelle von uns
 *  vorgegeben. Sie erzeugt die SMS-Elemente, füllt diese und übergibt
 *  sie an Ihre Funktion push. Später ruft sie die Elemente wieder mit
 *  Ihrer Funktion pop ab, gibt sie am Bildschrim aus und gibt
 *  abschließend ihren Speicher wieder frei. All das brauchen Sie
 *  nicht zu machen. Sie benötigen in dieser Aufgabe also kein malloc!
 *
 *  Ihre Aufgabe besteht lediglich darin, die bestehenden Elemente in
 *  die Liste einzufügen und aus ihr wieder zu entfernen. All das sind
 *  einfach Zeigeroperationen:
 *
 *  push: Hängen Sie in dieser Funktion das Element new an die Liste
 *    queue hinten (nach dem letzten Element) an. Prüfen Sie dazu
 *    zuerst, ob die Liste leer ist (queue->front und queue->back
 *    gleich NULL); dieser Fall erfordert eine gesonderte
 *    Behandlung. Stellen Sie in jedem Fall auch sicher, dass der
 *    Zeiger next des letzten Elements in der verketteten Liste den
 *    Wert NULL hat.
 *
 *  pop: Nehmen Sie in dieser Funktion das erste Element aus der Liste
 *    heraus und geben Sie einen Zeiger darauf als Rückgabewert
 *    zurück. (Das entnommene Element ist nach dieser Funktion nicht
 *    mehr Teil der Liste!) Sollte die Liste bereits zu Anfang leer
 *    sein (siehe oben), geben Sie NULL zurück. Sollte die Liste nach
 *    der Entnahme leer sein, setzten Sie bitte beide Listenzeiger
 *    (queue->front und queue->back) auf NULL.
 *
 *********************************************************************/
 

#include <stdio.h>
#include <stdlib.h>
 

struct SMS {
	char number[14];
	char text[161];
 
	struct SMS *next;
};
 

struct Queue {
	struct SMS *front;
	struct SMS *back;
};
 

void push(struct Queue *queue, struct SMS *new)
{
	if ( queue->front == NULL && queue->back == NULL ) {
		queue->front = new; queue->back = new;
	} else {
		queue->back->next = new; queue->back = new;
	}
	new->next = NULL;
}


struct SMS *pop(struct Queue *queue)
{
	if ( queue->front == NULL && queue->back == NULL )  return NULL;
	else {
		struct SMS *sms = queue->front;
		queue->front = sms->next;
		if ( queue->front == NULL ) {
			queue->back = NULL;
		} else {
			sms->next = NULL;
		}
		return sms;
	}
}
