/*********************************************************************
 *
 *  Aufgabe 407d
 *
 *  Die main-Funktion ist bereits vorgegeben. Lassen Sie diese bitte
 *  unbedingt unverändert. Implementieren Sie vielmehr die vier
 *  Funktionen datumEinlesen, tageDurchJahr, tageDurchMonat und
 *  istSchaltjahr, deren Gerüst unten bereits vorgegeben ist. Beachten
 *  Sie bitte die Kommentare, die vor jeder Funktion deren Aufgabe
 *  erklären.
 *
 *  Lassen Sie das vorgegebene Grundgerüst unverändert. Behalten Sie
 *  insbesondere die Funktionsdefinitionen und deren Reihenfolge
 *  bei. Lesen Sie auch die Hinweise auf dem Arbeitsblatt, ehe Sie
 *  diese Aufgabe bearbeiten.
 *
 *********************************************************************/


#include <stdio.h>


/* Feld zur Zuordnung zwischen der Nummer eines Wochentags und dessen
	 Namen (Zeichenfolge) */
const char *wochentage[7] = {
	"Sonntag",
	"Montag",
	"Dienstag",
	"Mittwoch",
	"Donnerstag",
	"Freitag",
	"Samstag"
};

/* Feld zur Zuordnung zwischen der Nummer eines Monats und der Anzahl
	 der Tage in diesem Monat */
const int monatstage[13] = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};


/*
 * Vorwärtsdeklarationen von Funktionen
 */
int datumEinlesen(int *jahr, int *monat, int *tag);
int tageDurchJahr(int jahr);
int tageDurchMonat(int monat, int jahr);
int istSchaltjahr(int jahr);

int isValidDay(int tag, int monat, int jahr);



int main()
{
	int jahr = 0, monat = 0, tag = 0;  /* Eingegebenes Datum */
	int tage = 0;          /* Tage die seit dem 1.1.0 vergangen sind */


	/*
	 * Abfragen des Datums
	 */
	if( !datumEinlesen(&jahr, &monat, &tag) )
	{
		return 1;
	}


	/*
	 * Berechnung der seit dem 1.1.0 vergangenen Tage
	 */
	tage = tageDurchJahr(jahr);
	tage += tageDurchMonat(monat, jahr);
	tage += tag - 1;  /* Anzahl der Tage durch die Tagesangabe */


	/*
	 * Ausgabe des Wochentags
	 */

	printf("Der %02i.%02i.%i ist ein %s.\n",
		tag, monat, jahr, wochentage[tage%7]);

	return 0;
}


/*
 * datumEinlesen
 *
 * Diese Funktion liest das Datum ein und speichert die Eingabe in den
 * mit jahr, monat und tag referenzierten Variablen. War die Eingabe
 * des Benutzers korrekt, gibt die Funktion 1 (wahr) zurück. Macht der
 * Benutzer eine falsche Eingabe (z.B. 29.2.2001 oder Monat > 12),
 * dann bricht die Funktion sofort ab und gibt 0 (falsch) zurück.
 */
int datumEinlesen(int *jahr, int *monat, int *tag)
{
	printf("Jahr: ");
	scanf("%i", jahr);
	if ( *jahr < 0 )  return 0; // 0 (falsch)

	printf("Monat: ");
	scanf("%i", monat);
	if ( *monat < 1 || *monat > 12 )  return 0; // 0 (falsch)

	printf("Tag: ");
	scanf("%i", tag);
	if ( !isValidDay(*tag, *monat, *jahr) )  return 0; // 0 (falsch)

	return 1; // 1 (wahr)
}


/*
 * tageDurchJahr
 *
 * Diese Funktion gibt die Anzahl der Tage zurück, die seit dem 1.1.0
 * bis zum Ende des Jahres vor der Jahresangabe jahr vergangen sind,
 * also die Anzahl an Tagen, die allein sich dadurch ergibt das man die
 * Jahreszahl angibt.
 */
int tageDurchJahr(int jahr)
{
	int vTage = 0;
	for ( int j = 0; j < jahr; j++ ) { vTage += ( istSchaltjahr(j) ) ? 366 : 365; }
  	return vTage;
}


/*
 * tageDurchMonat
 *
 * Diese Funktion gibt die Anzahl der Tage zurück, die im Jahr jahr
 * seit Jahresbeginn bis zum Ende des Monats vor der Monatsangabe
 * monat vergangen sind, also die Anzahl an Tagen, die sich dadurch
 * ergibt, dass man einen bestimmten Monat angibt. Der Parameter jahr
 * ermöglicht der Funktion zwischen Schaltjahren und Nicht-Schaltjahren
 * zu unterscheiden.
 */
int tageDurchMonat(int monat, int jahr)
{
	int vTage = 0;
	for ( int m = 1; m < monat; m++ ) { vTage += ( istSchaltjahr(jahr) && m == 2 ) ? 29 : monatstage[m]; }
	return vTage;
}


/*
 * istSchaltjar
 *
 * Berechnet, ob jahr ein Schaltjahr war. Trifft dies zu, gibt
 * die Funktion 1 zurück, andernfalls 0.
 */
int istSchaltjahr(int jahr)
{
	return ( jahr <= 0 ) ? 0 : ( jahr % 400 == 0 ) ? 1 : ( jahr % 100 == 0 ) ? 0 : ( jahr % 4 == 0 ) ? 1 : 0;
}


/**
 * isValidDay, checks if a given day is valid or not.
 * 0 : FALSE (invalid)
 * 1 : TRUE (valid, gueltiger Tag)
 */
int isValidDay(int tag, int monat, int jahr) {
	return ( tag < 1 || tag > monatstage[monat] ) ? ( istSchaltjahr(jahr) && monat == 2 && tag == 29 ) ? 1 : 0 : 1;
}

