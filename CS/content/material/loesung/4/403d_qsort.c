/*********************************************************************
 *
 *  Aufgabe 403d
 *
 *  Verwirklichen Sie den Quicksort-Algorithmus in C. Schreiben Sie
 *  dazu die Funktion Quicksort.
 *
 *  Die main-Funktion ist bereits vorgegeben. Sie kümmert sich um das
 *  Einlesen und Ausgeben der Zahlen. Lassen Sie diese Funktion bitte
 *  unbedingt unverändert. Implementieren Sie lediglich eine Funktion
 *  Quicksort nach obigem Algorithmus.
 *
 *********************************************************************/


#include <stdio.h>


int Feld[100];
void Quicksort(int links, int rechts);


int main()
{
	int i=0, j=0;

	/* Einlesen der Liste */
	do
	{

		printf("Bitte geben Sie eine Zahl ein oder Null zum Abbruch: ");
		scanf("%i", &Feld[i]);

		if (Feld[i] == 0)  break;

		++i;

	} while (i < 100);

	/* Sortieren der Liste */
	Quicksort(0, i-1);

	/* Ausgeben der sortierten Liste */
	for(j = 0; j < i-1; ++j)
	{
		printf("%i  ", Feld[j]);
	}

	printf("%i\n", Feld[i-1]);

	return 0;
}


/**
 * Implementierung von QuickSort
 */
void Quicksort(int links, int rechts) {
  int i = links, j = rechts;
  int x = Feld[(links + rechts) / 2]; // Pivotelement
  while ( i < j ) { // !(i >= j)
    for ( ; Feld[i] < x; i++ );
    for ( ; Feld[j] > x; j-- );
    if ( i <= j ) {
      int tmp = Feld[i]; // Vertausche
      Feld[i] = Feld[j]; // Feld[i] und
      Feld[j] = tmp;     // Feld[j].
      ++i; // i bewegt sich weiter nach rechts.
      --j; // j bewegt sich weiter nach links.
    }
  }
  // Rekursive Aufrufe von QuickSort
  if ( links < j )   Quicksort(links, j);
  if ( i < rechts )  Quicksort(i, rechts);
}

