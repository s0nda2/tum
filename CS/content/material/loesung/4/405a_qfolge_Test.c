/*********************************************************************
 *
 *  Aufgabe 405a
 *
 *  Verfassen Sie nun einen Algorithmus, der die n-te Q-Zahl in
 *  iterativer Form berechnet, indem Sie die Funktion
 *
 *    int qIterativ(int n)
 *
 *  implementieren. Der Parameter n gibt dabei die Nummer der Q-Zahl
 *  an, die Ihre Funktion berechnen und als Rückgabewert an main
 *  zurückgeben soll. Die aufrufende Funktion (main) stellt sicher,
 *  dass n eine beliebige ganze Zahl größer als Null ist.
 *
 *  Die main-Funktion ist bereits vorgegeben. Sie befindet sich in
 *  einer anderen Datei und übernimmt alle Ein- und Ausgaben. Sie
 *  müssen also nur noch die Berechnung implementieren.
 *
 *********************************************************************/


#include <stdio.h>
#include <stdlib.h>


int qIterativ(int n)
{
	if ( n <= 2 )  return 1;

	/* Ab hier gilt: n > 2 */
	int i = 2, *q;

	/* Speicher reservieren */
	q = (int*) malloc(n * sizeof(int));

	/* Eintragen der (zwei) Anfangswerte */
	q[0] = q[1] = 1;

	/* Iteratives Berechnen der n-ten Q-Zahl */
	do {
		q[i] = q[i-q[i-1]] + q[i-q[i-2]];
		++i;
	} while ( i < n );

	/* Wertzuweisung bevor Speicher freigeben */
	i = q[n-1]; // i-1

	/* Freigeben des reservierten Speichers */
	free(q);

	/* Ausgeben der Q-Zahl */
	return i;
}


int main() {
	int n;

	/* Nummer der gewuenschten Q-Zahl einlesen */
	printf("Nummer der gewuenschten Q-Zahl: ");
	scanf("%i", &n);

	/* Überprüfe, ob die eingegebene Nummer groesser als Null ist */
	if ( n <= 0 )  return 1;

	/* Ausgeben der Q-Zahl */
	printf("Iterativ Loesung: %i\n", qIterativ(n));

	return 0;
}
