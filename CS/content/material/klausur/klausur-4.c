/**
 * Programmieren in C
 *
 * Klausuraufgabe 4:
 *   In dieser Aufgabe geht es um die Caesar-Kodierung.
 *   Dabei gibt der Benutzer seinen zu verschlüsselnden Text und 
 *   eine positive ganze Zahl als Schlüssel an. Sie brauchen nicht
 *   zu überprüfen, dass der Schlüssel groesser als 0 ist.
 *
 *   Der Text wird  mithilfe des Schlüssels verschlüsselt, indem 
 *   jedes Zeichen des Textes um die Stellen, die der Schlüssel 
 *   vorgibt, verschoben wird. Wenn der Schlüssel 3 ist, dann wird 
 *   z.B. das Zeichen 'a' um 3 Stellen Richtung 'z' verschoben, 
 *   also aus 'a' wird 'd'. Und der Text 'alle' wird zu 'dooh', 
 *   bei dem Schlüssel 3.
 *   Falls bei der Verschlüsselung das Ergebnis eines Zeichens über 
 *   'z' hingeht, dann wird bei 'a' weiter verschoben.
 *   Zum Beispiel, für Schlüssel 3, wird 'y' nach 'b' konvertiert,
 *   da 'y' => 'z' => 'a' => 'b'.
 *
 *   Schreiben Sie ein Programm, das einen vom Benutzer eingegebenen
 *   Text mit Caesar-Kodierung verschlüsselt und anschließend den
 *   kodierten Text ausgibt. Der Text soll nicht länger als 100 Zeichen
 *   sein. Geben Sie einen Zeielenumbruch aus, bevor das Programm
 *   beendet wird.
 *
 */


#include <stdio.h>
#include <string.h>


int main() 
{
	char text[100];
	int key;

	/* Zu verschluesselnden Text einlesen */
	printf("Zu verschluesselnder Text: ");
	scanf("%s", text); // Beachte, dass der Text keinen Leerzeichen
	// dazwischen enthält. Sonst wird das Ergebnis falsch, da scanf
	// nur solange Zeichen einliest, bis ENTER oder ein Leerzeichen
	// erreicht wird. Bsp.: "ab cd" wird vom scanf nur als "ab" 
	// wahrgenommen. Besser: verwende fgets.

	/* Laenge des Textes berechnen */
	int laenge = (int) strlen(text);

	/* Schluessel einlesen */
	printf("Schluessel: ");
	scanf("%i", &key);

	/* Caesar-Verschluesselung */
	int i = 0;
	do {
		int diff = 'z' - text[i];
		text[i] = ( diff < key ) ? 'a' + ( key - diff - 1 ) : text[i] + key;
		++i;
	} while ( i < laenge );

	/* Verschluesselten Text ausgeben */
	printf("Ergebnis: %s\n", text);

	return 0;
}
