/**
 * Programmieren in C
 *
 * Klausuraufgabe 3:
 *   Gegeben sei die Zahlenfolge: 0 1 3 6 10 15 21 ...
 *
 *   Rechnungsvorschrift:  
 *
 *         x_i = i + x_(i-1)
 *
 *   wobei i der Index und x_i die Zahl mit Index i ist.
 *   
 *   Die Folge fängt beim Index 0 mit dem Wert 0 an,
 *   d.h. x_0 = 0. Die nächste Zahl x_1 rechnet sich 
 *   wie folgt:
 *   
 *         x_1 = 1 + x_0 = 1 + 0 = 1
 *
 *   Dann ist die Zahl x_2 mit Index 2 so:
 *
 *         x_2 = 2 + x_1 = 2 + 1 = 3
 *   
 *   usw.
 *   
 *   Sie brauchen die 'main' Funktion nicht zu schreiben.
 *   Sie wurde bereits in Brad implementiert.
 *   Alle Funktionen für die Ein- und Ausgaben befinden sich
 *   schon in 'main'. Lediglich die Funktion 'berechnen' (s.u.) 
 *   ist von Ihnen zu füllen.
 *
 *   Die Funktion 'berechnen' hat einen Parameter 'laenge', der
 *   Anzahl der zu berechnenden Zahlen der Folge angibt.

 *   Als Erstes sollen Sie in der Funktion 'berechnen' dynamisch
 *   Speicher für eine Zahlenfolge der Länge 'laenge' reservieren.
 *   Das können Sie mittels 'malloc' realisieren.
 *
 *   Initialisieren Sie das erste Element der Folge mit dem Wert 0
 *   und implementieren die Berechnung für nachfolgende Zahlen in der
 *   Folge. Anschliessend geben Sie den Zeiger auf das erste Element
 *   der Folge als Rückgabewert zurück.
 *
 *   Den reservierten Speicher brauchen Sie nicht mit 'free' freizugeben.
 */

#include <stdio.h>
#include <stdlib.h>


 int *berechnen(int laenge) {
	int *folge;

	/* Speicherplatz reservieren */
	folge = (int*) malloc(laenge * sizeof(int));

	/* Erstes Element mit 0 initialisieren */
	folge[0] = 0;

	/* Berechnungsvorschrift implementieren */
	for ( int i = 1; i < laenge; ++i ) {
		folge[i] = i + folge[i-1];
	}

	return folge;
}


/**
 * Folgende main Funktion wird nur zum Testzweck implementiert.
 */
int main() {
	int laenge, *folge;

	/* Laenge der Zahlenfolge einlesen */
	printf("Laenge: ");
	scanf("%i", &laenge);

	/* Variable 'laenge' muss groesser als Null sein. */
	if ( laenge <= 0 )  return -1;

	/* Zahlenfolge berechnen und Zeiger auf das erste 
	 * Folgenglied bekommen */
	folge = berechnen(laenge);

	/* Alle Folgengleider ausgeben */
	for ( int i = 0; i < laenge; ++i ) {
		printf("%5i", folge[i]);
	}
	
	/* Reservierten Speicher freigeben */
	free(folge);

	printf("\n");

	return 0;
}


