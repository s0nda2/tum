/**
 * Programmieren in C
 *
 * Klausuraufgabe 1 :
 *   Der Benutzer gibt in der Konsole ein Zeichen ein.
 *   Das Programm gibt an, ob das eingelesene Zeichen 
 *   ein Buchstabe, eine Zahl oder ein Sonderzeichen ist.
 *
 *   Alle Zeichen 'a' bis 'z' bzw. 'A' bis 'Z' sind Buchstaben.
 *   Alle Zeichen '0' bis '9' sind Zahlen.
 *   Alle anderen Zeichen sind als Sonderzeichen anzunehmen.
 *
 *   Hinweise (Ascii):
 *      '0' - '9'  :  48 - 57
 *      'A' - 'Z'  :  65 - 90
 *	'a' - 'z'  :  97 - 122
 */

 #include <stdio.h>


 int main() {
 	char c;

 	/* Zeichen einlesen */
	printf("Zeichen: ");
	scanf("%c", &c);

	/* Fallunterscheidung zwischen Buchstabe, Zahl und Sonderzeichen */
	if ( (c >= 65 && c <= 90) || (c >= 97 && c <= 122) ) { printf("Buchstabe");} 
	else if ( c >= 48 && c <= 57 ) { printf("Zahl"); } 
	else { printf("Sonderzeichen"); }

	printf("\n");

	return 0;
 }
