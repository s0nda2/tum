/**
 * Programmieren in C
 *
 * Klausuraufgabe 2:
 *   Diese Aufgabe ist die 1:1 Aufgabe in der Übung ("206h_binaer.c",
 *   und "402d_binaer.c"). Die Loesung ist identisch mit "402d_binaer.c".
 *
 */


void dez2bin(int deizmal, char binaerString[32])
{
	int aktbit;
	for ( int i = 30; i >= 0; --i ) {
		aktbit = (dezimal >> i) & 0x00000001;
		binaerString[30-i] = '0' + aktbit;
		// Oder: binaerString[30-i] = 48 + aktbit;
		// weil '0' hat Stelle 48 in Ascii.
	}
}
