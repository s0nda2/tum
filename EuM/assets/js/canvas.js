/**
 * Author: Soni D.
 * Language: JavaScript 
 */

var DrawObject = function () {
  this.init = function (x, y) {
    this.x = x;
    this.y = y;
  };
  this.draw = function () {}; 
}; // DrawObject


var PathObject = function () {
  DrawObject.call(this);
  // End points
  this.ex = 0;
  this.ey = 0;
  // Properties
  this.bent = 0; // 0 : straight, < 0 : bent left, > 0 : bent right
  // Implement draw function
  this.draw = function () {
    if ( this.bent == 0 ) {
      
    }
  }
}; // DirectionVector


function drawLine (ctx, sx, sy, ex, ey) {
  ctx.beginPath();
  ctx.moveTo(sx,sy);
  ctx.lineTo(ex,ey);
  ctx.closePath();
  ctx.stroke();
} // drawLine


/*
 * @params:
 *   ctx : canvas context
 *   sx  : x axis of start point
 *   sy  : y axis of start point
 *   ex  : x axis of end point
 *   ey  : y axis of end point
 *   bd  : bending direction.
 *         0   : straight (no bend),
 *         < 0 : bend left, 
 *         > 0 : bend right 
 */
function drawCurve (ctx, sx, sy, ex, ey, bd) {
  ctx.beginPath();
  ctx.moveTo(sx,sy);
  var cp1x = 0;
  if ( bd == 0 ) {
    
  } else if ( bd < 0 ) { // bend left
    
  } else { // bend right

  }
  ctx.bezierCurveTo(cp1x, cp1y, cp2x, cp2y, ex, ey);
}
