/**
 * main.js
 * Soni D.
 */

var addEventListenerThemen = function () {

  var g = { pageNaviId     : "page_navi",
            pageContentId  : "page_content",
            aLinkSelector  : 'ul:first-of-type li:not(:first-of-type) > a',
            themenSelector : 'div.themen',
            pageNavi       : null,
            pageContent    : null,
            aLinks         :  [],
            themen         : []
          };

  function init() {
    g.pageNavi = document.getElementById(g.pageNaviId) ? document.getElementById(g.pageNaviId) : null;
    g.pageContent = document.getElementById(g.pageContentId) ? document.getElementById(g.pageContentId) : null; 

    if ( g.pageNavi )
      g.aLinks = g.pageNavi.querySelectorAll(g.aLinkSelector) ? g.pageNavi.querySelectorAll(g.aLinkSelector) : [];

    if ( g.pageContent )
      g.themen = g.pageContent.querySelectorAll(g.themenSelector) ? g.pageContent.querySelectorAll(g.themenSelector) : [];

    return ( g.pageNavi && g.aLinks.length > 0 && g.aLinks.length ==  g.themen.length ) ? true : false;
  } // init


  function openThema ( k ) {
    console.log("Arbeitsblatt " + k + " clicked !");
    g.themen[k].style.visibility = "visible";
    for ( var j = 0; j < g.themen.length; j++ ) {
      if ( j != k ) g.themen[j].style.visibility = "hidden";
    }
  } // openThema

  
  if ( init() ) {
    console.log("aLinks.length : " + g.aLinks.length);
    console.log("themen.length : " + g.themen.length);   
    for ( var i = 0; i < g.aLinks.length; i++ ) {
      g.aLinks[i].addEventListener("click", function ( event ) {
        for ( var j = 0; j < g.aLinks.length; j++ ) {
          if ( event.target === g.aLinks[j] ) {
            openThema(j);
            break;
          }
        }
      }, false);
    }
  }

}; // addEventListenerThemen

window.onload = addEventListenerThemen;
