
public class MatrixMultOptimization {

	/**
	 * Sei A_i eine nxm-Matrix. Dann sind die Funktionen d1 und d2 wie folgt definiert:
	 * d1(i) := n,  und
	 * d2(i) := m
	 * 
	 * f(i,j) := 0, if i = j
	 * f(i,j) := min{ f(i,x) + f(x + 1,j) + (d1(i) * d2(x) * d2(j)) | i <= x < j },  if i < j
	 * 
	 * @param mm
	 * @return
	 */
    public static int f(int[][] mm) {
        return f(mm, 0, mm.length - 1);
    }

    public static int f(int[][] mm, int i, int j) {
        // TODO
    	if ( i == j ) return 0;
    	return min(mm, i, i, j);
    }
    
    
    /**
     * Diese hilfsfunktion bestimmt das Minimum einer Sequenz von Elementen.
     * 
     * @param mm
     * @param i
     * @param x
     * @param j
     * @return
     */
    private static int min(int[][] mm, int i, int x, int j) {
    	/* Letztes Element f(j-1) wird unmittelbar ausgegeben, wenn x = j-1 */
    	if ( x == j-1 ) return  f(mm, i, j-1) + f(mm, j, j) + ( mm[i][0] * mm[j-1][1] * mm[j][1] );
    	/* Erstes Element */
    	int f1 = f(mm, i, x) + f(mm, x+1, j) + ( mm[i][0] * mm[x][1] * mm[j][1] );
    	/* Zweites (bzw. naechstes) Element wird rekursiv bestimmt */
    	int f2 = min(mm, i, x+1, j);
    	/* Vergleich */
    	return ( f1 < f2 ) ? f1 : f2;
    }

    
    /*
    public static void main(String[] args) {
    	 int[][] mm = new int[][]{{10, 30}, {30, 5}, {5, 60}};
    	 System.out.println(f(mm));
    }*/
	
}
