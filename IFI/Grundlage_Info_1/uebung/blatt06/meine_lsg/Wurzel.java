
public class Wurzel extends MiniJava {

	/*
	 * Heron-Verfahren:  x_{n+1} = 1/2 * ( x_{n} + a / x_{n} )
	 * wobei
	 * 		a   : Zahl, deren Wurzel bestimmt werden soll
	 * 		x_0 : Beliebige positive Zahl (Startwert)  
	 * 
	 * x_0 kann aus der Taylorreihen-Entwicklung der binomische Reihen wie folgt 
	 * qualifiziert geschaetzt werden:
	 * 
	 * 		x_0 := (a + 1) / 2
	 * 
	 */	
	
	/**
	 * Heron-Verfahren (Iteration):  x_{n+1} = 1/2 * ( x_{n} + a / x_{n} )	 * 
	 * @param x
	 */	
	public static double heronIte(double a, double x, double e) {
		while ( Math.abs(x*x - a) >= e ) {
			x = 0.5 * (x + a / x);
		}
		return x;
	}
	
	
	/**
	 * Heron-Verfahren (Rekursion):  x_{n+1} = 1/2 * ( x_{n} + a / x_{n} )	 * 
	 * @param x
	 */
	public static double heronRec(double a, double x, double e) {
		if ( Math.abs(x*x-a) >= e ) {
			return heronRec(a, 0.5 * ( x + (a / x)), e);
		}
		return x;
	}
	
	
	/**
	 * Der Startwert x0 soll eine beliebige positive Zahl sein.	 *  
	 * @return Startwert x0
	 */
	private static double getStartVal(double a) {
		double startVal = 0.0;
		switch (new java.util.Random().nextInt() % 2) {
			case 0 :
				startVal = (new java.util.Random()).nextDouble() * (1 + (new java.util.Random()).nextInt(100));
				break;
			case 1 :
				startVal = (a + 1.0) / 2.0;
				break;
			default :
				break;
		}
		return startVal;
	}
	
	
	/**
	 * Heron-Verfahren:  x_{n+1} = 1/2 * ( x_{n} + a / x_{n} )	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		double a, e; // Quadratzahl a und Error e	
		a = e = 0.0;
		
		while ( a <= 0 ) { a = readDouble("Bitte geben Sie eine positive Zahl ein, "
				+ "deren Wurzel bestimmt werden soll!"); }
		while ( e <= 0 ) { e = readDouble("Bitte geben Sie eine nicht-negative Zahl "
				+ "als Fehlerintervall ein!"); }
		
		// Heron Recursion
		write("<html>Wurzel von <font color=\"red\">" + a + "</font> ist<br />"
				+ "(Iterativ) <font color=\"blue\">" + heronIte(a,getStartVal(a),e) + "</font><br />"
				+ "(Rekursiv) <font color=\"red\">" + heronRec(a,getStartVal(a),e) + "</font></html>");
				
		
	}
}
