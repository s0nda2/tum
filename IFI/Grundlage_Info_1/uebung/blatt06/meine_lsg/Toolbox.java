
public class Toolbox extends MiniJava {

	/**
	 * Diese Methode berechnet die Summe aller ganzen geraden Zahlen von 0 bis einschliesslich n,
	 * falls n >= 0, oder von n bis 0 , falls n < 0 ist. Als arithmetische Operatoren sind nur 
	 * die Additions- bzw. Subtraktionsoperatoren + und - erlaubt (die binaeren sowie unaeren 
	 * Varianten jeweils). D.h. insbesondere der Multiplikationsoperator * sowie *= sind nicht 
	 * erlaubt. Gehen Sie davon aus, dass n € [-10^4, +10^4]
	 * Beispiel: evenSum(-8) liefert -20 als Ergebnis.
	 *  
	 * @param n
	 * @return
	 */
    public static int evenSum(int n) {
        // TODO    	
    	int sum = 0;
    	if ( n > 0 ) {
    		if ( !isEven(n) )	n -= 1;
    		sum = n + evenSum(n-2);
    	} else if ( n < 0 ) {
    		if ( n % 2 != 0 )	n += 1;
    		sum = n + evenSum(n+2);
    	}
    	return sum;
    }

    
    /**
     * Hilfsmethode ueberprueft, ob die gegebene Zahl n gerade ist.
     * 
     * @param n
     * @return
     */
    private static boolean isEven( int n ) {
    	return ( n == 0 ) ? true : ( n == 1 || n == -1 ) ? false : ( n > 1 ) ? isEven(n-2) : /* n < -1 */ isEven(n+2); 
    }
    
    
    /**
     * Diese Methode multipliziert zwei Integer x und y multipliziert und liefert das Ergebnis zurueck.
     * Als arithmetische Operatoren sind nur die Additions- bzw. Subtraktionsoperatoren + und - erlaubt 
     * (die binaeren sowie unaeren Varianten jeweils). D.h. insbesondere der Multiplikationsoperator * 
     * sowie *= sind nicht erlaubt. Gehen Sie davon aus, dass n € [-10^4, +10^4]
     * 
     * @param x
     * @param y
     * @return
     */
    public static int multiplication(int x, int y) {
        // TODO
    	if ( x == 0 || y == 0 )	return 0;
    	if (x < 0 && y < 0) {
			x = -x;
			y = -y;
		}    	
    	return ( x > 0 && y > 0 ) ? ( x >= y ) ? x + multiplication(x,y-1) : y + multiplication(x-1,y)
			 : ( x < 0 && y > 0 ) ? x + multiplication(x,y-1) : y + multiplication(x-1, y);
    }

    
    /**
     * Diese Methode erwartet ein int-Array m als ersten Parameter und vertauscht die Eintraege in dem 
     * selbigen Array wie folgt: Sei m ein Array der Form [ a1 ,a2 ,...,an ], dann soll nach dem Aufruf 
     * reverse(m) das Array m die Form [ an ,...,a2 ,a1 ] haben. D.h. die (Hilfs-)Methode verändert das 
     * Array m und erstellt keine neuen Arrays (auch keine temporaeren).
     * 
     * @param m
     */
    public static void reverse(int[] m) {
        // TODO
    	if ( m != null && m.length > 0 ) {
    		swap(m, 0, m.length-1);
    	}    	
    }
    
    private static void swap(int[] m, int startIndex, int endIndex) {
    	// Hilfsfunktion fuer reverse
    	if ( startIndex >= endIndex ) return;
    	int temp = m[startIndex];
    	m[startIndex] = m[endIndex];
    	m[endIndex] = temp;
    	swap(m, startIndex + 1, endIndex - 1);
    }

    
    /**
     * Diese Methode erwartet ein int-Array m als ersten Parameter und gibt die Anzahl an ungeraden 
     * Integern aus dem Array m zurueck. Das Array m darf nicht veraendert werden.
     * Beispiel: Sei int[] m = new int[]{4, 7, 42, 5, 1, -5, 0, -4, -3}, dann liefert numberOfOddIntegers(m) 
     * als Ergebnis 5.
     * 
     * @param m
     * @return
     */
    public static int numberOfOddIntegers(int[] m) {
        // TODO
    	return ( m != null && m.length > 0 ) ? NOOI(m, 0) : 0;
    }
    
    private static int NOOI(int[] m, int i) {
    	if ( i == m.length ) return 0;
    	return ( m[i] % 2 != 0 ) ? 1 + NOOI(m, i+1) : NOOI(m, i+1);
    }

    
    /**
     * Diese Methode erwartet ein int-Array m als ersten Parameter und liefert ein int-Array zurueck. 
     * Das Ergebnis-Array enthaelt genau alle ungeraden Integer aus dem Array m. Des Weiteren spiegelt das 
     * Ergebnis-Array die Ordnung der Elemente aus dem Array m wider. D.h. hat das Array m an der Stelle i 
     * und j mit i < j ungerade Integer x und y, dann finden sich die Integer x und y in dem Ergebnis-Array 
     * an Stellen l und k wieder, sodass l < k gilt. Das Array m darf nicht veraendert werden.
     * Beispiel: Sei int[] m = new int[]{4, 7, 42, 5, 1, -5, 0, -4, -3}, dann liefert filterOdd(m) als Ergebnis 
     * ein Array, welches aequivalent zu folgendem ist: new int[]{7, 5, 1, -5, -3}.
     *  
     * @param m
     * @return
     */
    public static int[] filterOdd(int[] m) {
        // TODO
    	if ( m == null && m.length <= 0 ) return null;
    	int[] mm = new int[numberOfOddIntegers(m)];
    	FO(m, 0, mm, 0);
    	return mm;
    }
    
    /**
     * Hilfsfunktion fuer filterOdd(int[] m)
     * 
     * @param m  : Array
     * @param i  : (Start-)Index des Arrays m
     * @param mm : Ergebnis-Array 
     * @param j  : (Start-)Index des Arrays mm
     */
    private static void FO(int[] m, int i, int[] mm, int j) {
    	if ( i == m.length ) return;
    	if ( m[i] % 2 != 0 ) { mm[j++] = m[i]; }
    	FO(m, i+1, mm, j);
    }
    
}
