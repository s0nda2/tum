
public class Pi2 extends MiniJava {

	/**
	 * Diese Methode berechnet iterativ eine Annaeherung der Zahl PI.
	 * @param n
	 * @return
	 */
	public static double fnIte(long n) { // n >= 0
		double pi = 4.0; // f0 = 4.0
		long i = 0;
		while ( i < n ) {
			++i;
			if ( i % 2 == 0 ) { // n gerade
				pi = 4.0/(2.0*i+1) + pi;
			} else { // n ungerade
				pi = -4.0/(2.0*i+1) + pi;
			}
		}
		return pi;
	}
	
	/**
	 * Diese Methode berechnet rekursiv eine Annaeherung der Zahl PI.	 * 
	 * @param n
	 * @return
	 */
	public static double fnRec(long n) { // n >= 0
		return ( n == 0 ) ? 4.0 : ( n != 0 && n % 2 == 0 ) ? 4.0/(2.0*n+1) + fnRec(n-1) : -4.0/(2.0*n+1) + fnRec(n-1); 
	}
	
	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		long n = -1;
		while ( n < 0 ) {
			n = readInt("Geben Sie eine positive ganze Zahl ein!");
		} // n >= 0

		System.out.println("Iterativ: " + fnIte(n));
		System.out.println("Rekursiv: " + fnRec(n));
	}
}
