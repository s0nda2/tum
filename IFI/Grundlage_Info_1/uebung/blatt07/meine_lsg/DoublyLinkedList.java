
public class DoublyLinkedList {

    Entry head;
    int size;

    /**
     * constructor empty DoublyLinkedList
     */
    public DoublyLinkedList() {
        //TODO
    	head = null;
    	size = 0;
    }
    
    
    /**
     * Returns the number of elements in this list.
     * @return number of elements in this list
     */
    public int size(){
        //TODO
    	return size;
    }    

    /**
     * Appends a new element with value info to the end of this list
     * @param info value of the new element
     */
    public void add(int info) {
        //TODO
    	if ( head == null ) {
    		head = new Entry(null, null, info);
    	} else { // head != null
    		add(head, info);
    	}
    	++size;
    	
    }

    private void add(Entry node, int info) {
    	if ( node.next == null ) {
    		node.next = new Entry(node, null, info);
    	} else { // node.next != null
    		add(node.next, info);
    	}
    }
    
    
    /**
     * Inserts a new element with value info
     * at the specified position in this list.
     * @param index position where the element is inserted, from 0 ... list.size()-1
     * @param info value of the new element
     */
    public void add(int index, int info) {
        //TODO
    	if ( index < 0 || index > size ) return;
    	if ( index == size ) add(info); 
    	else {
    		Entry node = head;
    		for ( int i = index; i > 0; --i ) node = node.next;
    		Entry newNode = new Entry(node.prev, node, info);
    		node.prev = newNode;
    		if ( index > 0 ) newNode.prev.next = newNode;
    		if ( index == 0 ) head = newNode;
    		++size;
    	}
    }

    /**
     * Removes and returns the element at position index from this list.
     * @param index position of the element that is removed
     * @return value of the removed element
     */
    public int remove(int index) {
        //TODO
    	if ( index < 0 || index >= size ) return Integer.MIN_VALUE;
    	Entry node = head;
    	int elem = node.elem;
    	for ( int i = 0; i < index; i++ ) node = node.next;
    	if ( index > 0 ) node.prev.next = node.next;
    	if ( index == 0 ) head = node.next;
    	if ( index < size - 1) node.next.prev = node.prev;
    	node = node.prev = node.next = null;
    	--size;
    	return elem;
    }
    
    /**
     * shifts the list the specified number of positions to the left
     * example: [1,5,6,7] ---shift(2)---> [6,7,1,5]
     * @param index number of position to shift, from 0 to size-1
     */
    public void shiftLeft(int index){
        //TODO
    	if ( index <= 0 || index >= size ) return;
    	if ( head == null || head.next == null ) return;
    	Entry last = head;
    	// Determines the last entry
    	while ( last.next != null ) last = last.next;
    	// Connects head and last entry
    	head.prev = last;
    	last.next = head;
    	// Makes cyclic movements
    	for ( int i = 0; i < index; i++ ) head = head.next;
    	head.prev = head.prev.next = null;
    }
    
    /**
     * Alternative zur Methode shiftLeft. Erweitert den Wertebereich
     * von index auf positives Unendliche (+inf).
     * Nicht effizient wie shiftLeft, wegen verschachtelter Schleifen. 
     * 
     * shifts the list the specified number of positions to the left
     * example: [1,5,6,7] ---shift(2)---> [6,7,1,5]
     *          [1,5,6,7] ---shift(5)---> [5,6,7,1]
     * @param index number of position to shift, from 0 to +inf
     */
    public void shiftLeft2(int index){
        //TODO
    	if ( index <= 0 ) return;
    	if ( head == null || head.next == null ) return;
    	int i = 0;
    	Entry last = null;
    	do {
    		last = head;
    		// Determines the last entry
    		while ( last.next != null ) last = last.next;
    		// Makes a cyclic movement
    		head.prev = last;
    		last.next = head;
    		head = head.next;
    		head.prev = head.prev.next = null;
    	} while ( ++i < index );
    }

    /**
     * Kurze Alternative zur Methode shiftLeft
     * @param index
     */
    public void shiftLeft3(int index){
        //TODO
    	if ( index <= 0 ) return;
    	int i = 0;
    	while ( i < index ) {
    		int elem = head.elem;
    		this.remove(0);
    		this.add(elem);
    		++i;
    	}
    }
    
    
    @Override
    public String toString() {
        String out = "[";
        if (head != null) {
            out += head.elem;
            Entry tmp = head.next;
            while (tmp != null) {
                out = out + "," + tmp.elem;
                tmp = tmp.next;
            }
        }
        out += "]";
        return out;
    }

        
    public static void main(String[] args) {
	//TODO 
    	DoublyLinkedList dll = new DoublyLinkedList();
    	System.out.println(dll.size());
    	dll.add(4);
    	dll.add(2);
    	dll.add(3);
    	System.out.println(dll.toString());
    	dll.add(0, 7);
    	System.out.println(dll.toString());
    	dll.add(1, 8);
    	System.out.println(dll.toString());
    	dll.add(1, 9);
    	System.out.println(dll.toString());
    	dll.remove(0);
    	System.out.println(dll.toString());
    	dll.shiftLeft(3);
    	System.out.println(dll.toString());
    }

    class Entry {

        Entry prev;
        Entry next;
        int elem;

        public Entry(Entry prev, Entry next, int elem) {
            this.prev = prev;
            this.next = next;
            this.elem = elem;
        }

    }
}
 
