
public class Fak {

	/**
	 * Rekursive (aber nicht endrekursiv) Implemetierung der Fakultaet.
	 * Nicht-endrekursiver Funktionsaufruf ist <<nicht>> die letzte
	 * Aktion der aufrufenden Funktion.
	 * 
	 * https://de.wikipedia.org/wiki/Endrekursion
	 * 
	 * @param n
	 * @return
	 */
	public static int facRec(int n) {
		return ( n <= 1 ) ? 1 : n * facRec(n-1);
		/*
		 * facRec ist nicht-endrekursiv, weil deren letzte Aktion die
		 * Multiplikation (*), und nicht facRec(n-1), ist.
		 *   
		 */
	}
	
	/**
	 * Endrekursive (Tail recursive) Implementierung der Fakultaet.
	 * Bei der Endrekursion ist die letzte Aktion immer der rekursiver
	 * Funktionsaufruf selbst (mit aktualisierten Parametern).
	 * 
	 * Endrekursion kann in Iteration umgewandelt werden.
	 * 
	 * https://de.wikipedia.org/wiki/Endrekursion
	 * 
	 * @param n
	 * @return
	 */
	public static int facTailRec(int n) {
		return ( n <= 1 ) ? 1 : facTailRecHelper(n, 1);
	}
	
	private static int facTailRecHelper(int n, int k) {
		return ( n == 1 ) ? k : facTailRecHelper(n-1, k*n);
	}
	
	/**
	 * Validation: 
	 * Sei n >= 2. Es gilt:
	 * facTailRec(n) = facTailRecHelper(n, 1)                , n > 1
	 *               = facTailRecHelper(n-1, 1*n)            , (*)
	 *               = ...
	 *               = facTailRecHelper(1, 1*n*(n-1)*...*2)  , (**)
	 *               = 1*n*(n-1)*...*2
	 *               = n*(n-1)*...*2*1
	 *               = n!
	 * 
	 * Von (*) bis (**) sind es insg. (n-1) Schritte, in denen einfach nur
	 * die Parameter m und n von facTailRecHelper aktualisiert werden.
	 */
	
	
	/**
	 * Iterative Implementierung der Fakultaet.
	 * Endrekursion kann in Iteration umgewandelt werden.
	 * 
	 * @param n
	 * @param k
	 * @return
	 */
	public static int facIt(int n, int k) {
		while ( true ) {
			if ( n <= 1 ) 
				return k;
			else
				k *= n--; // k = k * n; n = n - 1;
		}
	}
}
