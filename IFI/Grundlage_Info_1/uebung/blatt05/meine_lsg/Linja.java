import java.io.ObjectInputStream.GetField;


public class Linja extends MiniJava {

	
    private static int[][] spielfeld = new int[8][6];

    
    /**
     * initialisiert das Spielfeld
     * Ziellinie fuer Spieler 1 ist Zeile 7
     * Ziellinie fuer Spieler -1 ist Zeile 0
     */
    private static void initSpiel() {
        for (int i = 0; i < spielfeld.length; i++) {
            if (i != 0 && i != spielfeld.length - 1) {
                spielfeld[i] = new int[]{-(12 - i + 1), 0, 0, 0, 0, 6 + i};
            }
            if (i == 0) {
                spielfeld[i] = new int[]{1, 2, 3, 4, 5, 6};
            }
            if (i == spielfeld.length - 1) {
                spielfeld[i] = new int[]{-6, -5, -4, -3, -2, -1};
            }
        }
    }

    
    /**
     *
     * @return formatiertes aktuelles Spielfeld
     */
    private static String output() {
        String tmp = "Spieler 1 spielt von oben nach unten\n"
                + "Spieler -1 spielt von unten nach oben\n";
        for (int i = 0; i < spielfeld.length; i++) {
            for (int j = 0; j < spielfeld[i].length; j++) {
                tmp = tmp + "\t" + spielfeld[i][j];
            }
            tmp = tmp + "\n";
        }
        return tmp;
    }

    
    /**
     * @return true, wenn die Eingabe stein im richtigen Wertebereich liegt und
     * zum Spieler gehoert; false, sonst
     */
    private static boolean gueltigeEingabe(int stein, int spieler) {
    	// TODO
    	return ( spieler == 1 && stein >= 1 && stein <= 12) || ( spieler == -1 && stein >= -12 && stein <= -1 ) ? true : false;
    }

    
    /**
     * @param stein kann Werte -1 bis -12 und 1 bis 12 haben
     * @return gibt x-Koordinate von stein an Position 0 und die y-Koordinaten
     * von stein an Position 1 zurueck; falls stein nicht gefunden, wird {-1,-1}
     * zurueckgegeben
     */
    private static int[] findeStein(int stein) {
        //TODO
    	boolean gefunden = false;
    	int[] pos = new int[]{-1,-1};
    	for ( int i = 0; i < spielfeld.length && !gefunden; i++ ) {
    		for ( int j = 0; j < spielfeld[i].length && !gefunden; j++ ) {
    			if ( spielfeld[i][j] == stein ) {
    				pos[0] = i; pos[1] = j; // (x,y) = (i,j) = (Reihe, Spalte)
    				gefunden = true; // terminates the inner and outer loop
    			}
    		}
    	}
    	return pos; // pos[0] = x = Reihe, pos[1] = y = Spalte
    }

    
    /**
     * @param reihe hat Werte 0 bis 7
     * @return Anzahl der Steine in einer Reihe
     */
    private static int steineInReihe(int reihe) {
    	// TODO
    	int n = 0;
    	for ( int j = 0; j < spielfeld[reihe].length; j++ ) {
    		if ( spielfeld[reihe][j] != 0 )
    			n = n + 1;
    	}
    	return n;
    }

    
    /**
     * Ueberprueft, ob der Zug zulaessig ist und fuehrt diesen aus, wenn er
     * zulaessig ist.
     *
     * @param vorwaerts == true: Zug erfolgt vorwaerts aus Sicht des
     * Spielers/Steins vorwearts == false: Zug erfolgt rueckwaerts aus Sicht des
     * Spielers/Steins
     * @return Rueckgabe -1: Zug nicht zulaessig Rueckgabe 0-5: Weite des
     * potentiellen naechsten Zugs (falls Folgezug folgt) Rueckgabe 6: Ziellinie
     * wurde genau getroffen (potentieller Bonuszug)
     *
     */
    private static int setzeZug(int stein, int weite, boolean vorwaerts) {
    	//TODO
    	
    	/*
    	 * Rueckgabe 0-5 entspricht der Anzahl der Steine in der Reihe, auf die der aktuelle
    	 * Spieler mit seinem ziehenden Stein trifft.
    	 * Dann kann er im potentiellen naechsten (Folge-)Zug um 0-5 Reihen vorwaertsspringen.
    	 */
    	
    	if ( weite <= 0 ) return -1; // Der Einfachheitshalber unterbinde negative weite
    	
    	int[] pos = findeStein(stein); // Aktuelle Position von stein : (pos[0], pos[1]) = (Reihe, Spalte)
    	int naechsterZug = -1;
    	
    	/* Es ist egal, ob der Spieler gerade den Anfangs- oder Folgezug macht.
		 * Hauptsache ist, wenn er mit seinem Stein seine Ziellinie mit passender Shrittzahl 
		 * erreicht, bekommt er immer einen Bonuszug (als Folgezug).
		 * Ist der Spieler im Anfangszug und setzt seinen Stein mit passender Schrittzahl
		 * in die Ziellinie, erhaelt er dafuer keinen Folgezug, stattdessen einen Bonuszug.
		 * Damit ist sein Zug beendet und der andere Spieler beginnt seinen Zug.
		 * Ist der Spieler im Folgezug und setzt seinen Stein mit passender Schrittzahl
		 * in die Ziellinie, erhaelt er einen Bonuszug. Dann ist sein Zug beendet.
		 */
    	if ( stein >= 1 && stein <= 12 ) { // Spieler 1
    		if ( vorwaerts ) { // spielt vorwaerts (von oben nach unten), d.h. von Reihe 0 bis 7
    			if ( pos[0] == -1 && pos[1] == -1 ) { // Aktueller Stein in der Ziellinie
    				naechsterZug = -1; // Stein in der Ziellinie kann nicht weiter vorwaerts ziehen.
    			} else { // Aktueller Stein nicht in Ziellinie
    				if ( pos[0] + weite ==  spielfeld.length - 1 ) { // pos[0] + weite == 7 == Ziellinie
    					naechsterZug = 6; // Bonuszug
    					/* Steine, die die Ziellinie erreicht oder ueberschritten haben, werden auf dem 
    					 * Spielfeld nicht mehr dargestellt.
						 */
    					spielfeld[pos[0]][pos[1]] = 0; 
    				} else if ( pos[0] + weite >= spielfeld.length ) {
    					naechsterZug = ( weite == 1 ) ? /* Anfangszug oder Bonuszug */ -1 
    											  : /* Folgezug */ 0; // Restliche Schritte verfallen --> Anderer Spieler ist dran.
    					spielfeld[pos[0]][pos[1]] = 0;
    				} else { // pos[x] + weite < spielfeld.length - 1
    					if ( steineInReihe(pos[0] + weite) >= 6 ) {
    						naechsterZug = -1;
    					} else { // Reihe hat weniger als 6 Steine
    						naechsterZug = steineInReihe(pos[0] + weite); // Rueckgabe 0-5
    						setzeStein(stein, pos[0] + weite);
    					}
    				}
    			}
    		}
    		else { // rueckwaerts (ist nur im Bonuszug erlaubt)
    			if ( pos[0] == -1 && pos[1] == -1 ) { // Aktueller Stein in Ziellinie kann rueckwaerts ziehen
    				if ( steineInReihe(spielfeld.length - 2) >= 6 ) {
    					naechsterZug = -1;
    				} else { // Reihe hat weniger als 6 Steine
    					naechsterZug = steineInReihe(spielfeld.length - 2);
    					setzeStein(stein, spielfeld.length - 2);
    				}
    			} else { // Aktueller Stein nicht in Ziellinie
    				if ( weite > 1 || pos[0] - weite <= -1 ) {
    					naechsterZug = -1;
    				} else { // weite == 1
    					if ( pos[0] - weite == 0 ) { // Eigene Startlinie
    						if ( steineInReihe(0) >= 6 ) {
    							naechsterZug = -1;
    						} else {
    							naechsterZug = 0;
    							setzeStein(stein, 0);
    						}
    					} else { // pos[0] - weite != 0
    						if ( steineInReihe(pos[0] - weite) >= 6 ) {
    							naechsterZug = -1;
    						} else {
    							naechsterZug = 0;
    							setzeStein(stein, pos[0] - weite);
    						}
    					}
    				}
    			}
    		}
    	} else if ( stein <= -1 && stein >= -12 ) { // Spieler -1
    		if ( vorwaerts ) { // spielt vorwaerts (von unten nach oben), d.h. von Reihe 7 bis 0
    			if ( pos[0] == -1 && pos[1] == -1 ) { // Aktueller Stein in der Ziellinie
    				naechsterZug = -1; // Stein in der Ziellinie kann nicht weiter vorwaerts ziehen.
    			} else { // Aktueller Stein nicht in Ziellinie
    				if ( pos[0] - weite == 0 ) { // Ziellinie
    					naechsterZug = 6; // Bonuszug
    					spielfeld[pos[0]][pos[1]] = 0;
    				} else if ( pos[0] - weite < 0 ) {
    					naechsterZug = ( weite == 1 ) ? /* Anfangszug oder Bonuszug */ -1 
    													: /* Folgezug */ 0; // Restliche Schritte verfallen --> Anderer Spieler ist dran.
    					spielfeld[pos[0]][pos[1]] = 0;
    				} else { // pos[x] - weite > 0
    					if ( steineInReihe(pos[0] - weite) >= 6 ) {
    						naechsterZug = -1;
    					} else { // Zielreihe hat weiniger als 6 Steine
    						naechsterZug = steineInReihe(pos[0] - weite);
    						setzeStein(stein, pos[0] - weite);
    					}
    				}
    			}
    		}
    		else { // rueckwaerts (ist nur im Bonuszug erlaubt)
    			if ( pos[0] == -1 && pos[1] == -1 ) { // Aktueller Stein in Ziellinie kann rueckwaerts ziehen
    				if ( steineInReihe(1) >= 6 ) {
    					naechsterZug = -1;
    				} else { // Reihe hat weniger als 6 Steine
    					naechsterZug = steineInReihe(1);
    					setzeStein(stein, 1);
    				}
    			} else { // Aktueller Stein nicht in Ziellinie
    				if ( weite > 1 || pos[0] + weite >= spielfeld.length  ) {
    					naechsterZug = -1;
    				} else { // weite == 1
    					if ( pos[0] + weite == spielfeld.length - 1 ) { // Eigene Startlinie
    						if ( steineInReihe(spielfeld.length - 1) >= 6 ) {
    							naechsterZug = -1;
    						} else {
    							naechsterZug = 0;
    							setzeStein(stein, spielfeld.length - 1);
    						}
    					} else { // pos[0] + weite < spielfeld.length - 1
    						if ( steineInReihe(pos[0] + weite) >= 6 ) {
    							naechsterZug = -1;
    						} else {
    							naechsterZug = 0;
    							setzeStein(stein, pos[0] + weite);
    						}
    					}
    				}
    			}
    		}
    	}
    	return naechsterZug;
    }

    
    /**
     * @return true, falls die Bedingungen des Spielendes erfuellt sind, d.h.
     * alle Steine des einen Spielers sind an den Steinen des gegnerischen Spielers
     * vorbeigezogen
     *
     */
    private static boolean spielende() {
    	// TODO

    	/*
    	 * Spieler 1 spielt von oben nach unten. Ziellinie fuer Spieler 1 ist Zeile 7.
         * Spieler 2 spielt von unten nach oben. Ziellinie fuer Spieler -1 ist Zeile 0.
    	 */
    	boolean endeStatus = true;
    	for ( int i = 0; i < spielfeld.length && endeStatus; i++ ) {
    		int stein = 0;
    		for ( int j = 0; j < spielfeld[i].length; j++ ) {
    			if ( spielfeld[i][j] != 0 ) {
    				if ( stein == 0 ) {
    					stein = spielfeld[i][j];
    				} else {
    					if ( spielfeld[i][j] > 0 && stein < 0 || spielfeld[i][j] < 0 && stein > 0 ) {
    						endeStatus = false;
    						break;
    					}
    				}
    			}
    		}
    	}
    	return endeStatus;
    }

    
    /**
     * zaehlt die Punkte der beiden Spieler und gibt das Ergebnis aus
     */
    private static void zaehlePunkte() {
    	//TODO
    	if ( spielende() ) {
    		int punktSpieler1, punktSpieler2, anzStein, multiplikator;
    		punktSpieler1 = punktSpieler2 = 0;
    		
    		/* Spieler 1 */
    		outerLoop:
        	for ( int i = spielfeld.length - 1; i >= 0; i-- ) {
        		anzStein = 0;
        		multiplikator = getMultiplikator(1, i);
        		for ( int j = 0; j < spielfeld[i].length; j++ ) {
        			if ( spielfeld[i][j] < 0 ) { break outerLoop; }
        			else if ( spielfeld[i][j] > 0 ) { anzStein += 1; }
        		}
        		punktSpieler1 += multiplikator * anzStein;
        	}
    		
    		/* Spieler -1 */
    		outerLoop2:
    		for ( int i = 0; i < spielfeld.length; i++ ) {
    			anzStein = 0;
        		multiplikator = getMultiplikator(-1, i);
    			for ( int j = 0; j < spielfeld[i].length; j++ ) {
    				if ( spielfeld[i][j] > 0 ) { break outerLoop2; }
    				else if ( spielfeld[i][j] < 0 ) { anzStein += 1; }
    			}
    			punktSpieler2 += multiplikator * anzStein;
    		}
    		
    		/* Vergleich der Punkte */
    		String output = "Spieler 1 : " + punktSpieler1 + " Punkte\n" +
    						"Spieler 2 : " + punktSpieler2 + " Punkte\n";
    		output += ( punktSpieler1 > punktSpieler2 ) ? "Spieler 1 hat gewonnen!" 
    				: ( punktSpieler1 < punktSpieler2 ) ? "Spieler 2 hat gewonnen" 
    				: "Gleichstand!";
    		write(output);
    	}    	
    }

    
    /**
     * Spielablauf entsprechend Anfangszug, Folgezug, Bonuszug
     *
     * @param spieler ist 1 (Spielsteine 1 bis 12) oder -1 (Spielsteine -1 bis
     * -12)
     */
    private static void spielerZieht(int spieler, boolean bonus) {
    	//TODO
    	int stein, weite, tmp, richtung; // richtung : 0 -> rueckwaerts, 1 -> vorwaerts
    	
    	write("\nSpieler " + spieler + " ist am Zug.");
    	System.out.println("Spieler " + spieler + " ist am Zug.");
    	
    	stein = readInt("<html>ANFANGSZUG<br /><br />W&auml;hlen Sie einen Stein!</html>");
        while ( !gueltigeEingabe(stein, spieler) ) {
        	stein = readInt("<html>Es muss einer Ihrer eigenen Steine sein.<br />W&auml;hlen Sie einen Stein!</html>");
        }
        
        /* Anfangszug */
        System.out.println("ANFANGSZUG (Spieler " + spieler + ")");
    	weite = setzeZug(stein, 1, true);    // Anfangszug wird ausgefuehrt 	
    	while ( weite == -1 ) { // Ungueltiger Zug
    		write("<html>Der Zug ist kein g&uuml;ltiger Zug.</html>");
    		stein = readInt("<html>ANFANGSZUG<br /<br />W&auml;hlen Sie einen anderen Stein!</html>");
            while ( !gueltigeEingabe(stein, spieler) ) {
            	stein = readInt("<html>Es muss einer Ihrer eigenen Steine sein.<br />W&auml;hlen Sie einen Stein!</html>");
            }
    		weite = setzeZug(stein, 1, true);
    	}
    	System.out.println(output());
    	
    	/* Folgezug oder Bonuszug */
    	System.out.println("FOLGEZUG (Spieler " + spieler + ")");
    	if ( weite >= 1 && weite <= 5 ) { // Folgezug
    		stein = readInt("<html>FOLGEZUG<br /><br />W&auml;hlen Sie einen Stein!</html>");
            while ( !gueltigeEingabe(stein, spieler) ) {
            	stein = readInt("<html>Es muss einer Ihrer eigenen Steine sein.<br />W&auml;hlen Sie einen Stein!</html>");
            }
            tmp = weite;
    		weite = setzeZug(stein, tmp, true); // Folgezug wird ausgefuehrt
    		while ( weite == -1 ) { // Ungueltiger Zug
        		write("<html>Der Zug ist kein g&uuml;ltiger Zug.</html>");
        		stein = readInt("<html>FOLGEZUG<br /<br />W&auml;hlen Sie einen anderen Stein!</html>");
                while ( !gueltigeEingabe(stein, spieler) ) {
                	stein = readInt("<html>Es muss einer Ihrer eigenen Steine sein.<br />W&auml;hlen Sie einen Stein!</html>");
                }
        		weite = setzeZug(stein, tmp, true);
        	}
    		System.out.println(output());
    		if ( weite == 6 ) { // Bonuszug (nach Folgezug)
    			if ( bonus ) {
    				System.out.println("BONUSZUG (Spieler " + spieler + ")");
    				stein = readInt("<html>BONUSZUG<br /><br />W&auml;hlen Sie einen Stein!</html>");
                	while ( !gueltigeEingabe(stein, spieler) ) {
                		stein = readInt("<html>Es muss einer Ihrer eigenen Steine sein.<br /> W&auml;hlen Sie einen Stein!</html>");
                	}
                	richtung = readInt("<html>BONUSZUG<br /><br />Wollen Sie Ihren Stein vorw&auml;rts- oder r&uuml;ckw&auml;rts ziehen?<br />" 
										+ "Geben Sie <b>1</b>(vorw&auml;rts) oder <b>0</b> (r&uuml;ckw&auml;rts) ein!</html>");
                	while ( !( richtung == 0 || richtung == 1) ) {
                		richtung = readInt("<html>Ihre letzte Eingabe <font color=\"red\">" + richtung + "</font> war falsch.<br />"  
                							+ "Geben Sie <b>1</b>(vorw&auml;rts) oder <b>0</b> (r&uuml;ckw&auml;rts) ein!</html>");
                	}
                	weite = ( richtung == 1 ) ? setzeZug(stein, 1, true) : setzeZug(stein, 1, false);
                	while ( weite == -1 ) { // Ungueltiger Zug
                		write("<html>Der Zug ist kein g&uuml;ltiger Zug.</html>");
                		stein = readInt("<html>BONUSZUG<br /<br />W&auml;hlen Sie einen anderen Stein!</html>");
                        while ( !gueltigeEingabe(stein, spieler) ) {
                        	stein = readInt("<html>Es muss einer Ihrer eigenen Steine sein.<br />W&auml;hlen Sie einen Stein!</html>");
                        }
                		weite = ( richtung == 1 ) ? setzeZug(stein, 1, true) : setzeZug(stein, 1, false);
                	}
                	System.out.println(output());
    			}
    		}
    		weite = 0; // Zug beendet --> Anderer Spieler ist dran
    	} else if ( weite == 6 ) { // Bonuszug (direkt nach Anfangszug)
    		if ( bonus ) {
    			System.out.println("BONUSZUG (Spieler " + spieler + ")");
    			stein = readInt("<html>BONUSZUG<br /><br />W&auml;hlen Sie einen Stein!</html>");
            	while ( !gueltigeEingabe(stein, spieler) ) {
            		stein = readInt("<html>Es muss einer Ihrer eigenen Steine sein.<br /> W&auml;hlen Sie einen Stein!</html>");
            	}
            	richtung = readInt("<html>BONUSZUG<br /><br />Wollen Sie Ihren Stein vorw&auml;rts- oder r&uuml;ckw&auml;rts ziehen?<br />" 
									+ "Geben Sie <b>1</b>(vorw&auml;rts) oder <b>0</b> (r&uuml;ckw&auml;rts) ein!</html>");
            	while ( !( richtung == 0 || richtung == 1) ) {
            		richtung = readInt("<html>Ihre letzte Eingabe <font color=\"red\">" + richtung + "</font> war falsch.<br />"  
            							+ "Geben Sie <b>1</b>(vorw&auml;rts) oder <b>0</b> (r&uuml;ckw&auml;rts) ein!</html>");
            	}
            	weite = ( richtung == 1 ) ? setzeZug(stein, 1, true) : setzeZug(stein, 1, false);
            	while ( weite == -1 ) { // Ungueltiger Zug
            		write("<html>Der Zug ist kein g&uuml;ltiger Zug.</html>");
            		stein = readInt("<html>BONUSZUG<br /<br />W&auml;hlen Sie einen anderen Stein!</html>");
                    while ( !gueltigeEingabe(stein, spieler) ) {
                    	stein = readInt("<html>Es muss einer Ihrer eigenen Steine sein.<br />W&auml;hlen Sie einen Stein!</html>");
                    }
            		weite = ( richtung == 1 ) ? setzeZug(stein, 1, true) : setzeZug(stein, 1, false);
            	}
            	System.out.println(output());
    		}
    		weite = 0;
    	}
    }

    
    /**
     * 
     */
    private static int getMultiplikator(int stein, int reihe) {
    	int m = 0;
    	if ( stein >= 1 && stein <= 12 ) { // Spieler 1
    		m = ( reihe == 0 ) ? -5 : ( reihe == 1 ) ? -3 : ( reihe == 2 ) ? -2 : ( reihe == 3 ) ? -1 :
    			( reihe == 4 ) ? 1  : ( reihe == 5 ) ?  2 : ( reihe == 6 ) ?  3 : 5;
    	} else if ( stein <= -1 && stein >= -12 ) { // Spieler -1
    		m = ( reihe == 0 ) ?  5 : ( reihe == 1 ) ?  3 : ( reihe == 2 ) ?  2 : ( reihe == 3 ) ? 1 :
    			( reihe == 4 ) ? -1 : ( reihe == 5 ) ? -2 : ( reihe == 6 ) ? -3 : -5;
    	}
    	return m;
    }
    
    
    /**
     * Eigene Methode.
     * Diese Methode setzt den gew&uuml;nschten Stein auf eine freie Stelle in der Reihe
     */
    private static void setzeStein(int stein, int reihe) {
    	int[] pos = findeStein(stein);
    	int i = 0;
    	while ( i < spielfeld[reihe].length && spielfeld[reihe][i] != 0 ) { i += 1; }
    	if ( i < spielfeld[reihe].length ) {
    		spielfeld[reihe][i] = stein; // Stein auf neue Position setzen
    		spielfeld[pos[0]][pos[1]] = 0; // Alte Position freigeben
    	}
    }
    
    
    /**
     * Hauptprogramm
     * @param args
     */
    public static void main(String args[]) {

        initSpiel();
        System.out.println(output());
        
        // TODO

        int input, spieler, stein, weite;
        boolean bonus;
        
    	spieler = 1; // beginnt das Spielt    	
    	input = readInt("<html>Wollen Sie mit Bonusregel spielen?<br />Geben Sie <b>1</b> (Bonus) oder <b>0</b> (kein Bonus) an!</html>");
    	while ( !( input == 0 || input == 1 ) ) {
    		input = readInt("<html>Ihre letzte Eingabe <font color=\"red\">" + input + "</font> war falsch.<br />" + 
							"Geben Sie <b>1</b> (Bonus) oder <b>0</b> (kein Bonus) an!</html>");
    	}
    	
    	bonus = ( input == 1 ) ? true : false;    	
    	do {
    		spielerZieht(spieler, bonus); // Spieler 1
    		spieler *= -1;
    		spielerZieht(spieler, bonus); // Spieler -1
    		spieler *= -1;
    	} while ( !spielende() );
    	
    	zaehlePunkte();

    }
}
