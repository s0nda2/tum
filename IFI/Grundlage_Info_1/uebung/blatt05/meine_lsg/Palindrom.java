
public class Palindrom extends MiniJava {

	/* ASCII
	 * 		[A..Z] : 65-90
	 * 		[a..z] : 97-122
	 */
	
	/**
	 * Diese Methode liefert als Rückgabewert einen String, der die gleiche Zeichenfolge wie input ist,
	 * wobei alle Grossbuchstaben A bis Z in den entsprechenden Kleinbuchstaben umgewandelt sind
	 * 
	 * @param input
	 * @return
	 */
	public static String toLowerCase(String input) {
		if ( input == null ) return null;
		String output = "";
		if ( input.length() > 0 ) { // !input.isEmpty() 
			for ( int i = 0; i < input.length(); i++ )
				output += ( input.charAt(i) >= 'A' && input.charAt(i) <= 'Z' ) ? (char) (input.charAt(i) + 'a' - 'A') : input.charAt(i);
		}
		return output;
	}
	
	
	/**
	 * Diese Methode wandelt den String input in ein char-Array um und dieses als Rückgabewert 
	 * zurueckgibt. Dabei gilt, dass der erste Buchstabe an der ersten Position im Array steht,
	 * der zweite Buchstabe an der zweiten Position usw.
	 * 
	 * @param input
	 * @return
	 */
	public static char[] toCharArray(String input) {
		if ( input == null || input.isEmpty() ) return null;
		char[] output = new char[input.length()];
		for ( int i = 0; i < input.length(); i++ ) {
			output[i] = input.charAt(i);
		}
		return output;
	}
	
	/**
	 * Diese Methode durchläuft das Array input mit Hilfe einer Schleife und bestimmt, ob es sich 
	 * bei der gesamten Zeichenkette um ein Palindrom handelt. Der Rückgabewert is true, wenn die 
	 * Zeichenkette ein Palindrom ist, ansonsten false. Überlegen Sie sich, ob Sie das Array in 
	 * seiner vollen Länge durchlaufen müssen, oder ob es reicht, über die Hälfte zu iterieren.
	 * 
	 * @param input
	 * @return
	 */
	public static boolean isPalindrome(char[] input) {
		if ( input == null || input.length == 0  ) return false;
		boolean b = true;
		int n = ( input.length % 2 == 0 ) ? input.length / 2 - 1 : (input.length - 1) / 2 - 1;
		for ( int i = 0; i < n && b; i++ ) {
			if ( input[i] != input[input.length - 1 - i] )
				b = false;
		}
		return b;
	}
	
	/**
	 * Lesen Sie mittels readString() einen nicht-leeren String vom Benutzer ein, für den Sie 
	 * mithilfe der zuvor implementierten Methoden überprüfen, ob es sich bei der Zeichenkette 
	 * um ein Palindrom handelt. Teilen Sie das Ergebnis dieser Überprüfung dem Benutzer mittels 
	 * write(String txt) mit.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		String input = readString();
		String output = toLowerCase(input);
		boolean b = isPalindrome(toCharArray(output));
		if ( b )
			write(input + " ist Palindrom.");
		else
			write(input + " ist KEIN Palindrom.");
	}
	
}
