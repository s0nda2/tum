
public class Arrays {
	
	/**
	 * Diese Methode gibt das Arrays feld auf der Konsole (System.out.println(String text)) aus. 
	 * Das Array soll mit einer oeffnenden geschweiften Klammer beginnen und mit einer 
	 * schließenden geschweiften Klammer enden; die einzelnen Elemente des Arrays sollen durch 
	 * ein Komma und Leerzeichen getrennt sein.
	 * 
	 * Beispiel: print(new int[] {1, 2, 3, 4, 5}) liefert auf der Konsole die Ausgabe 
	 * 				{1, 2, 3, 4, 5}.

	 * @param feld
	 */
	public static void print(int[] feld) {
		if ( feld == null )	return;
		String output = "{";
		for ( int i = 0; i < feld.length-1; i++ ) {	output += feld[i] + ", "; }
		if ( feld.length > 0 ) { output += feld[feld.length-1]; }
		output += "}";
		System.out.println(output);
	}
	
	/**
	 * Diese Methode gibt Minimum und Maximum des Arrays feld auf der Konsole 
	 * (System.out.println(String text)) aus. Das Array wird dabei nur einmal durchlaufen.
	 * 
	 * Beispiel: minUndMax(new int[] {1, 10, 25, -13, 1000}) liefert auf der Konsole die Ausgabe
	 * 				Minimum = -13, Maximum = 1000.
	 */
	public static void minUndMax(int[] feld) {
		if ( feld == null || feld.length == 0 ) return;		
		int min, max;
		max = min = feld[0];		
		for ( int i = 1; i < feld.length; i++ ) {
			if ( feld[i] < min ) min = feld[i];
			if ( feld[i] > max ) max = feld[i];
		}		
		System.out.println("Minimum = " + min + ", Maximum = " + max);
	}
	
	/**
	 * Diese Methode gibt ein neues Array zurück, das die Elemente von feld in umgekehrter 
	 * Reihenfolge enthält.
	 * Beispiel: invertieren(new int[] {0, 1, 2, 3}) liefert ein Array {3, 2, 1, 0} zurück.
	 */
	public static int[] invertieren(int[] feld) {
		if ( feld == null || feld.length == 0 ) return null;
		int[] iFeld = new int[feld.length];
		for ( int i = 0; i < iFeld.length; i++ ) {
			iFeld[i] = feld[feld.length-1-i];
		}
		return iFeld;		
	}
	
	/**
	 * Diese Methode gibt ein neues Array zurueck, dass laenge Zeichen lang ist und die 
	 * Elemente von feld in der gleichen Reihenfolge und so viele wie moeglich enthaelt. 
	 * Sollte das zurueckgegeben Feld groesser sein als das uebergebene, sollen die 
	 * zusaetzlichen Positionen den Wert 0 haben.
	 * 
	 * Beispiel: schneiden(new int[] {1, 2, 3}, 2) liefert ein Array {1, 2} und 
	 * schneiden(new int[] {1, 2, 3}, 5) liefert ein Array {1, 2, 3, 0, 0}.
	 */
	public static int[] schneiden(int[] feld, int laenge) {
		if ( laenge <= 0 ) return null;
		int[] sFeld = new int[laenge]; // array is initiated with zeros
		int lim = ( feld.length <= laenge ) ? feld.length : laenge;
		for ( int i = 0; i < lim; i++ ) {
				sFeld[i] = feld[i];
		}
		return sFeld;
	}
	
	/**
	 * Diese Methode gibt ein neues eindimensionales Array zurueck, das die Werte des 
	 * uebergebenen zweidimensionalen Arraysfeld enthaelt, zurueck. 
	 * Die Zeilen des Arrays feld sollen dabei nacheinander in der ihrem Zeilenindex 
	 * entsprechenden Reihenfolge in dem eindimensionalem Array abgelegt werden. 
	 * Beachten Sie, dass Zeilen nicht gleich lang sein müssen.
	 * 
	 * Beispiel: linearisieren(new int[][] {{1, 3}, {25}, {7, 4, 6, 9}}) liefert ein 
	 * 				Array {1, 3, 25, 7, 4, 6, 9}.
	 */
	public static int[] linearisieren(int[][] feld) {
		if ( feld == null || feld.length == 0 ) return null;
		int k = 0;
		for ( int i = 0; i < feld.length; i++ ) {
			k += feld[i].length;
		}
		int[] linFeld = new int[k];
		k = 0;
		for ( int i = 0; i < feld.length; i++ ) {
			for ( int j = 0; j < feld[i].length; j++ ) {
				linFeld[k++] = feld[i][j];
			}
		}
		return linFeld;
	}
	
	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		print(linearisieren(new int[][]{{1,3},{25},{7,4,6,9},{3,4}}));
	}
}
