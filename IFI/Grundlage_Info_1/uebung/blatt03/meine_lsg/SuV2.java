
public class SuV2 extends MiniJava {
	
	public static void main(String[] args) {
		int[] player = new int[2]; // Punkte des jeweiligen Spielers
		int p; // 0 fuer Spieler 1, und 1 fuer Spieler 2
		int turns = 0; // Spiel laeuft solange turns < 2
		
		/* Jeder Spieler hat am Anfang zwei Karten */
		player[0] = drawCard();
		player[1] = drawCard();
		player[0] += drawCard();
		player[1] += drawCard();
		
		/* Default: Spieler 1 beginnt zuerst */
		p = 0;
		
		write("SPIEL BEGINNT\n\nJeder Spieler hat am Anfang 2 Karten.\n\n"
				+ "Spieler 1 : " + player[0] + " Punkte\n"
				+ "Spieler 2 : " + player[1] + " Punkte");
		while ( turns < 2 ) {
			String msg = "<font color=\"green\">************ SPIELER </font>"
							+ "<font color=\"red\">" + (p+1) + "</font>"
							+ "<font color=\"green\"> IST AM ZUG ************</font><br /><br />";
			msg += ( p == 0 ) ? "Spieler 1 : <font color=\"red\">" + player[0] + "</font> Punkte <br />"
							  + "Spieler 2 : " + player[1] + " Punkte"
							  : "Spieler 1 : " + player[0] + " Punkte<br />"
							  + "Spieler 2 : <font color=\"red\">" + player[1] + "</font> Punkte";
			write("<html>" + msg + "</html>");
			if ( player[0] >= 22 || player[1] >= 22 ) {
				turns = 2; // Break out while loop
			} else {
				msg += "<br /><br />Wollen Sie weitere Karte ziehen? [0:Nein, 1:Ja]";
				int choose = read("<html>" + msg + "</html>");
				while ( choose < 0 || choose > 1  ) {  choose = read("<html>" + msg + "</html>");	}
				if ( choose == 0 ) { // 0 : Keine weitere Karte ziehen
					p = 1 - p; // Anderen Spieler wechseln
					turns++;
				} else { // 1 : Weitere Karte ziehen
					player[p] += drawCard();					
				}
			}
		}
		
		
		write("<html><font color=\"green\">************ ENDERGEBNIS ************</font><br /><br />"
				+ "Spieler 1 : " + player[0] + " Punkte <br />"
				+ "Spieler 2 : " + player[1] + " Punkte");
		
		if ( player[0] >= 22 && player[1] >= 22 ) {
			write("Spieler 1 gewinnt.");
		} else if ( player[0] >= 22 ) { 
			write("Spieler 2 gewinnt.");
		} else if ( player[1] >= 22 ) {
			write("Spieler 1 gewinnt.");
		} else {
			if ( player[0] == player[1] ) {
				write("Spieler 1 gewinnt."); // Haben beide Spieler gleich viele Punkte, so gewinnt der erste Spieler.
			} else if ( player[0] > player[1] ) {
				write("Spieler 1 gewinnt.");
			} else {
				write("Spieler 2 gewinnt.");
			}
		}
	}
	
}
