
public class SuV extends MiniJava {

	public static void main(String[] args) {
		int p, // Spieler 1 (p == 0), Spieler 2 (p == 1)
			player1, player2, // Punkte des jeweiligen Spielers
			input, 
			status; // Spiel laeuft weiter, solange status < 2
		
		status = player1 = player2 = 0;
		
		/* Jeder Spieler hat am Anfang zwei Karten */
		player1 += drawCard();
		player2 += drawCard();
		player1 += drawCard();
		player2 += drawCard();
		
		/* Spieler 1 beginnt */
		p = 0; // Aktueller Spieler
		
		while ( status < 2 ) {
			String s = "***************** SPIELER " + (p+1) + " ist AM ZUG *****************\n\n"
				+ "Spieler 1 : " + player1 + " Punkte \n"
				+ "Spieler 2 : " + player2 + " Punkte";
			write(s);
			
			if ( player1 >= 22 || player2 >= 22 ) {
				status = 2;
			} else {
				s += "\n\nWollen Sie weitere Karten ziehen? [0 : Nein / 1 : Ja]";
				input = read(s);
				while ( input < 0 || input > 1 ) {
					input = read(s);
				}
				if ( input == 0 ) { // Spieler lehnt weitere Karten ab
					p = 1 - p;
					status++; 
				} else { // Spieler zieht weitere Karte
					if ( p == 0 )  player1 += drawCard();
					else  player2 += drawCard();
				}			
			}
		} // END WHILE
		
		if ( player1 >= 22 && player2 >= 22 ) {
			if ( p == 0 )  write("Spieler 1 gewinnt.");
			else  write("Spieler 2 gewinnt.");
		}
		else if ( player1 >= 22 )  write("Spieler 2 gewinnt."); 
		else if ( player2 >= 22 )  write("Spieler 1 gewinnt.");
		else {
			if ( player1 == player2 ) {
				if ( p == 0 ) write("Spieler 1 gewinnt. Weil er zuerst " + player1 + " erreicht hat.");
				else  write("Spieler 2 gewinnt. Weil er zuerst " + player2 + " erreicht hat.");
			} 
			else if (  player1 > player2 )  write("Spieler 1 gewinnt.");
			else write("Spieler 2 gewinnt.");
		}
		
	}
	
}
