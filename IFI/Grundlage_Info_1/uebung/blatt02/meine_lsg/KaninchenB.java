/**
 * 
 * Aufgabe 2.7 (H) Kaninchenpopulation (b)
 * 
 * Anfangs wird ein geschlechtsreifes Kaninchenpaar auf der Insel ausgesetzt.
 * Jedes Kaninchenpaar ist im ersten Monat geschlechtsreif.
 * Jedes Kaninchen hat genau 3 Monate Lebenszeit.
 * 
 * Kaninchenpaare, die sich im zweiten Monat ihrer Geschlechtsreife (2. Generation) befinden, erzeugen jeweils 3 neue Kaninchenpaare.
 * 
 * In allen anderen Generationen (1. und 3.) erzeugt ein Kaninchenpaar weiterhin nur ein neues Kaninchenpaar.
 *
 * +---------+-----------------+-----------------+-----------------+
 * |  Monat  |  1. Generation  |  2. Generation  |  3. Generation  |
 * +---------+-----------------+-----------------+-----------------+
 * |   1.    |       1         |                 |                 | Das erste Kaninchenpaar K1 ist anfangs schon schlechtsreif
 * |         |                 |                 |                 | K1 erzeugt ein neues Kaninchenpaar K2. 
 * |         |                 |                 |                 | (K2 ist noch nicht geschlechtsreif).
 * |         |                 |                 |                 | K2 ist die erste Erzeugung (Generation) von K1 ==> 1.Generation = 1
 * +---------+-----------------+-----------------+-----------------+
 * |   2.    |       1         |       3         |                 | Nach dem 1. Monat und w�hrend des 2. Monats ist K2 auch geschlechtsreif.
 * |         |                 |                 |                 | Nun gibt es 2 geschlechtsreife Paare, naemlich K1 und K2.
 * |         |                 |                 |                 | In diesem 2. Monat erzeugt K1 in seiner 2. Erzeugung drei neue Paare K3, K4, K5.
 * |         |                 |                 |                 | K2 erzeugt in seiner ersten Erzeugung nur das Kaninchenpaar K6.
 * |         |                 |                 |                 | (K3, K4, K5 und K6 sind noch nicht geschlechtsreif).
 * |         |                 |                 |                 | K3, K4, K5 sind die zweite Erzeugung (Generation) von K1 ==> 2. Generation = 3
 * |         |                 |                 |                 | K6 ist die erste Erzeugung (Generation) von K2 ==> 1. Generation = 1 
 * +---------+-----------------+-----------------+-----------------+
 * |   3.    |       4         |       3         |       1         | Nach dem 2. Monat und w�hrend des 3. Monats sind K3, K4, K5, K6 geschlechtsreif.
 * |         |                 |                 |                 | Es sind jetzt 6 geschlechtsreife Kaninchenpaare: K1, K2, K3, K4, K5, K6.
 * |         |                 |                 |                 | K1 erzeugt K7 ==> Dritte Erzeugung ==> 3. Generation = 1
 * |         |                 |                 |                 | K2 erzeugt K8, K9, K10 ==> Zweite Erzeugung ==> 2. Generation = 3
 * |         |                 |                 |                 | K3, K4, K5, K6 erzeugen jeweils in erster Erzeugung ein neues Paar (K11,..., K14)
 * |         |                 |                 |                 |     ==> 1. Generation = 1 + 1 + 1 + 1 = 4
 * |         |                 |                 |                 | K7,..., K14 sind noch nich geschlechtsreif.
 * +---------+-----------------+-----------------+-----------------+
 * |   4.    |       8         |       12        |       1         | Nach dem 3. Monat stirbt K1. (Lebenszeit von 3 Monate vollendet).
 * |         |                 |                 |                 | K7,..., K14 sind jetzt geschlechtsreif.
 * |         |                 |                 |                 | Es sind insg. 13 geschlechtsreife Kaninchenpaare K2, K3, .., K14.
 * |         |                 |                 |                 | K2 erzeugt K15 ==> Dritte Erzeugung ==> 3. Generation = 1
 * |         |                 |                 |                 | K3, K4, K5, K6 erzeugen jeweils in ihrer zweiten Erzeugung 3 neue Kaninchenpaare:
 * |         |                 |                 |                 |     (K16,K17,K18), (K19,K20,K21), (K22,K23,K24), (K25,K26,K27)
 * |         |                 |                 |                 |     ==> 2. Generation = 3 + 3 + 3 + 3 = 4*3 = 12
 * |         |                 |                 |                 | K7,..., K14 erzeugen jeweils in ihrer ersten Erzeugung (Generation) ein neues Paar:
 * |         |                 |                 |                 |     K28, ..., K35
 * |         |                 |                 |                 |     ==> 1. Generation = 1 + ... + 1 = 8*1 = 8
 * |         |                 |                 |                 | K15,..., K35 sind noch nicht geschlechtsreif.
 * +---------+-----------------+-----------------+-----------------+
 * |   5.    |       21        |       24        |       4         | Nach dem 4. Monat stirbt K2. (Lebenszeit von 3 Monate vollendet).
 * |         |                 |                 |                 | K15,..., K35 sind jetzt geschlechtsreif.
 * |         |                 |                 |                 | Es sind insg. 33 geschlechtsreife Kaninchenpaare K3, .., K35.
 * |         |                 |                 |                 | K3, K4, K5, K6 erzeugen jeweils in ihrer dritten Erzeugung (Generation) ein neues Paar:
 * |         |                 |                 |                 |     K36, ..., K39
 * |         |                 |                 |                 |     ==> 3. Generation = 1 + 1 + 1 + 1 = 4
 * |         |                 |                 |                 | K7,..., K14 erzeugen in ihrer zweiten Erzeugung (Generation) jeweils 3 neue Paare:
 * |         |                 |                 |                 |     (K40,K41,K42), ...., (K61,K62,K63)
 * |         |                 |                 |                 |     ==> 2. Generation = 8*3 = 24
 * |         |                 |                 |                 | K15,..., K35 erzeugen in ihrer ersten Erzeugung (Generation) jeweils ein neues Paar: 
 * |         |                 |                 |                 |     K64, ..., K84
 * |         |                 |                 |                 |     ==> 1. Generation = 21 * 1 = 21
 * |         |                 |                 |                 | K36,..., K84 sind noch nicht geschlechtsreif.
 * +---------+-----------------+-----------------+-----------------+
 * |   6.    |       49        |       63        |        8        | Nach dem 5. Monat sterben K3, K4, K5, K6. (Lebenszeit von 3 Monate vollendet)
 * |         |                 |                 |                 | K36,..., K84 sind nun geschlechtsreif.
 * |         |                 |                 |                 | Es sind insg. 78 geschlechtsreife Kaninchenpaare K7, ..., K84.
 * |         |                 |                 |                 | K7,..., K14 erzeugen in ihrer dritten Erzeugung (Generation) jeweils ein neues Paar:
 * |         |                 |                 |                 |     K85, ..., K92
 * |         |                 |                 |                 |     ==> 3. Generation = 8*1 = 8
 * |         |                 |                 |                 | K15,..., K35 erzeugen in ihrer zweiten Erzeugung (Generation) jeweils 3 neue Paare: 
 * |         |                 |                 |                 |     (K93,K94,K95), ..., (K153,K154,K155)
 * |         |                 |                 |                 |     ==> 2. Generation = 21 * 3 = 63
 * |         |                 |                 |                 | K36,..., K84 erzeugen in ihrer ersten Erzeugung (Generation) jeweils ein neues Paar:
 * |         |                 |                 |                 |     K156, ..., K204
 * |         |                 |                 |                 |     ==> 1. Generation = 49*1 = 49
 * |         |                 |                 |                 | K85,..., K204 sind noch nicht geschlechtsreif. 
 * +---------+-----------------+-----------------+-----------------+
 * 
 * Zusammenfassung : 
 *   Wir sehen, dass die Anzahl der 2. Generation im k-ten Monat dem 3fachen der Anzahl 
 *   der 1. Generation im (k-1)ten Monat entspricht.
 *   Das laesst sich dadurch erklaeren, dass jedes reife Kaninchenpaar in seinem 2ten Monat
 *   immer 3 neue Paare zur Welt bringt --> Das verdreifacht die Anzahl im letzten (ersten)
 *   Monat einfach.
 * 
 * 
 * Definiere:
 *   n : Monat, 
 *   j : Generation, wobei j = 1,2,3
 *   
 *   g(n,j) : Anzahl reifer Paare im n-ten Monat von j-ten Generation
 *   
 *   f(n) := 1                            , fuer n = 1,2
 *        := 4                            , fuer n = 3 
 *        := f(n-1) + 3*f(n-2) + f(n-3)   , sonst (n > 3)
 *          
 *                   
 *  Definiere die Funktionen:
 *    1)  Anzahl reifer Paare im n-ten Monat der 1. Generation:
 *    
 *            g(n,1) := f(n)
 *                   
 *    2)  Anzahl reifer Paare im n-ten Monat der 2. Generation:
 *    
 *            g(n,2) := 0                            , fuer n = 1
 *                   := 3 * g(n-1, 1)                , sonst ( n > 1 )
 *    
 *    3)  Anzahl reifer Paare im n-ten Monat der 3. Generation:
 *    
 *            g(n,3) := 0                            , fuer n = 1,2
 *					 := g(n-2,1)                     , sonst ( n > 2 )
 *
 *    4)  Anzahl aller reifen Paare im n-ten Monat:
 *    
 *            F(n) := 1                            , fuer n = 0,1
 *				   := 2                            , fuer n = 2
 *                 := F(n-1) + 3*F(n-2) + F(n-3)   , sonst ( n > 2 )
 *                   
 * +---------+-----------------+-----------------+-----------------+-----------------------+
 * |  Monat  |  1. Generation  |  2. Generation  |  3. Generation  |  Anzahl reifer Paare  |
 * |         |                 |                 |                 |        F(Monat)       | F(0) := 1
 * +---------+-----------------+-----------------+-----------------+-----------------------+
 * |   1.    |       1         |                 |                 |          1            | F(1) := 1
 * +---------+-----------------+-----------------+-----------------+-----------------------+
 * |   2.    |       1         |       3         |                 |          2            | F(2) := 2
 * +---------+-----------------+-----------------+-----------------+-----------------------+
 * |   3.    |       4         |       3         |       1         |          6            | F(3) = f(2) + 3*f(1) + f(0) = 2 + 3*1 + 1 = 6
 * +---------+-----------------+-----------------+-----------------+-----------------------+
 * |   4.    |       8         |       12        |       1         |          13           | F(4) = f(3) + 3*f(2) + f(1) = 6 + 3*2 + 1 = 13
 * +---------+-----------------+-----------------+-----------------+-----------------------+
 * |   5.    |       21        |       24        |       4         |          33           | F(5) = f(4) + 3*f(3) + f(2) = 13 + 3*6 + 2 = 33
 * +---------+-----------------+-----------------+-----------------+-----------------------+
 * |   6.    |       49        |       63        |       8         |          78           | F(6) = f(5) + 3*f(4) + f(3) = 33 + 3*13 + 6 = 78
 * +---------+-----------------+-----------------+-----------------+-----------------------+
 *          
 */
public class KaninchenB extends MiniJava {

	/**  
	 *  Hilfsfunktion:
	 *  
	 *    f(n) := 1                            , fuer n = 1,2
	 *         := 4                            , fuer n = 3 
	 *         := f(n-1) + 3*f(n-2) + f(n-3)   , sonst ( n > 3 )
	 */
	private static int f (int n) {
		return ( n < 3 ) ? 1 : ( n == 3 ) ? 4 : f(n-1) + 3*f(n-2) + f(n-3);
	}
	
	/**
	 *    1)  Anzahl reifer Paare im n-ten Monat der 1. Generation:
     *    
     *            g(n,1) := f(n)
     *                   
     *    2)  Anzahl reifer Paare im n-ten Monat der 2. Generation:
     *    
     *            g(n,2) := 0                            , fuer n = 1
     *                   := 3 * g(n-1, 1)                , sonst ( n > 1 )
     *    
     *    3)  Anzahl reifer Paare im n-ten Monat der 3. Generation:
     *    
     *            g(n,3) := 0                            , fuer n = 1,2
     *					 := g(n-2,1)                     , sonst ( n > 2 )
     *
	 * @param n : Monat
	 * @param j : Generation (1,2,3)
	 * @return Anzahl geschlechtsreifer Kaninchenpaare im n-ten Monat in der j-ten Generation.
	 */
	private static int g (int n, int j) {
		return ( j == 1 ) ? f(n) : ( j == 2 ) ? ( ( n == 1 ) ? 0 : 3 * g(n-1, 1) ) : ( n == 1 || n == 2 ) ? 0 : g(n-2, 1);
	}
	
	public static void main(String[] args) {
		int n = readInt("Geben Sie eine postive ganze Zahl f�r Monat ein: ");
		while ( n < 1 ) {
			n = readInt("Geben Sie eine postive ganze Zahl f�r Monat ein: ");
		}
		String s = "Monat : " + n + "\n"
				 + "1. Generation : " + g(n,1) + "\n"
				 + "2. Generation : " + g(n,2) + "\n"
				 + "3. Generation : " + g(n,3);
		write(s);
	}
	
}
