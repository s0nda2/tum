
public class Meiern2 extends MiniJava {

	/**
	 * 
	 * @param args
	 * 
	 * [Meier]  21 >
	 * [Pasch]  66 > 55 > 44 > 33 > 22 > 11 >
	 * [Normal] 65 > 64 > 63 > 62 > 61 > 54 > 53 > 52 > 51 > 43 > 42 > 41 > 32 > 31   
	 */
	public static void main(String[] args) {
		int player; // Spieler (0), Computer (1)
		int number, lastNumber, output;
		
		lastNumber = player = 0; // Spieler beginnt		
		number = 1;
		
		while ( number > lastNumber ) {
			lastNumber = number;
			
			int d1 = dice();
			int d2 = dice();
			number = ( d1 >= d2 ) ? 10*d1 + d2 : 10*d2 + d1;
			output = number;
			if ( d1 == d2 )  number *= 10;			
			
			if ( player == 0 ) write("Spieler hat " + output + " gew�rfelt.");
			else write("Computer hat " + output + " gew�rfelt.");
			
			if ( number == 21 )  lastNumber = number;
			else  player = 1 - player;
		}
		
		if ( player == 0 )  write("Spieler hat gewonnen.");
		else  write("Computer hat gewonnen.");
	}
}
