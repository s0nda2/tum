
public class GGTTable extends MiniJava {

	protected static int ggT (int x, int y) {
		while ( x != y ) {
			if ( x < y ) {
				y = y - x;
			} else {
				x = x - y;
			}
		}
		return x;
	}
	
	public static void main(String[] args) {
		int x = 0;
		while ( x < 1 ) {
			x = read("Geben Sie eine positive ganze Zahl ein:");
		}
		
		String s = "";
		
		for ( int i = 1; i <= x ; i++ ) {
			s += "\t" + i + "\t"; 
		}
		
		for ( int i = 1; i <= x; i++ ) {
			s += "\n" + i; 
			for ( int j = 1; j <= x; j++ ) {
				s += "\t" + ggT(i,j) + "\t";
			}
		}
		
		System.out.println(s);
	}
}
