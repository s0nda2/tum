
/**
 * 
 * Aufgabe 2.7 (H) Kaninchenpopulation (a)
 * 
 * Anfangs wird ein geschlechtsreifes Kaninchenpaar auf der Insel ausgesetzt.
 * Jedes Kaninchenpaar ist im ersten Monat geschlechtsreif.
 * Jedes Kaninchenpaar erzeugt jeden Monat ein neues Kaninchenpaar.
 * Jedes Kaninchen hat genau 3 Monate Lebenszeit. 
 *
 * +---------+-----------------+-----------------+-----------------+
 * |  Monat  |  1. Generation  |  2. Generation  |  3. Generation  |
 * +---------+-----------------+-----------------+-----------------+
 * |   1.    |       1         |                 |                 | Das erste Kaninchenpaar K1 ist anfangs schon schlechtsreif
 * |         |                 |                 |                 | K1 erzeugt ein neues Kaninchenpaar K2. (K2 ist noch nicht geschlechtsreif).
 * |         |                 |                 |                 | K2 ist die erste Erzeugung (Generation) von K1 ==> 1.Generation = 1
 * +---------+-----------------+-----------------+-----------------+
 * |   2.    |       1         |       1         |                 | Nach dem 1. Monat und w�hrend des 2. Monats ist K2 auch geschlechtsreif.
 * |         |                 |                 |                 | Nun gibt es 2 geschlechtsreife Paare, naemlich K1 und K2.
 * |         |                 |                 |                 | In diesem 2. Monat erzeugt K1 das Paar K3, und K2 das Paar K4.
 * |         |                 |                 |                 | (K3 und K4 sind noch nicht geschlechtsreif).
 * |         |                 |                 |                 | K3 ist die zweite Erzeugung (Generation) von K1 ==> 2. Generation = 1
 * |         |                 |                 |                 | K4 ist die erste Erzeugung (Generation) von K2 ==> 1. Generation = 1 
 * +---------+-----------------+-----------------+-----------------+
 * |   3.    |       2         |       1         |       1         | Nach dem 2. Monat und w�hrend des 3. Monats sind K3 und K4 geschlechtsreif.
 * |         |                 |                 |                 | Es sind jetzt 4 geschlechtsreife Kaninchenpaare K1, K2, K3, K4.
 * |         |                 |                 |                 | K1 erzeugt K5 ==> Dritte Erzeugung ==> 3. Generation = 1
 * |         |                 |                 |                 | K2 erzeugt K6 ==> Zweite Erzeugung ==> 2. Generation = 1
 * |         |                 |                 |                 | K3 erzeugt K7, K4 erzeugt K8, jeweils in erster Erzeugung ==> 1. Generation = 1 + 1 = 2
 * |         |                 |                 |                 | K5, K6, K7, K8 sind noch nich geschlechtsreif.
 * +---------+-----------------+-----------------+-----------------+
 * |   4.    |       4         |       2         |       1         | Nach dem 3. Monat stirbt K1. (Lebenszeit von 3 Monate vollendet).
 * |         |                 |                 |                 | K5, K6, K7, K8 sind nun geschlechtsreif.
 * |         |                 |                 |                 | Es sind insg. 7 geschlechtsreife Kaninchenpaare K2, K3, .., K8.
 * |         |                 |                 |                 | K5, K6, K7, K8 erzeugen jeweils in ihrer ersten Erzeugung (Generation) ein neues Paar.
 * |         |                 |                 |                 |   ==> 1. Generation = 1 + 1 + 1 + 1 = 4
 * |         |                 |                 |                 | K3, K4 erzeugen jeweils in ihrer zweiten Erzeugung (Generation) ein neues Paar.
 * |         |                 |                 |                 |   ==> 2. Generation = 1 + 1 = 2
 * |         |                 |                 |                 | K2 erzeugt in seiner dritten Erzeugung (Generation) ein neues Kaninchenpaar.
 * |         |                 |                 |                 |   ==> 3. Generation = 1
 * +---------+-----------------+-----------------+-----------------+
 * |   5.    |       7         |       4         |       2         |  usw.
 * +---------+-----------------+-----------------+-----------------+
 * |   6.    |       13        |       7         |       4         |
 * +---------+-----------------+-----------------+-----------------+
 * |   7.    |       24        |       13        |       7         |
 * +---------+-----------------+-----------------+-----------------+
 * 
 * Zusammenfassung :
 * 
 * +---------+-----------------+-----------------+-----------------+----------+
 * |  Monat  |  1. Generation  |  2. Generation  |  3. Generation  |  Gesamt  | f(0) := 1
 * +---------+-----------------+-----------------+-----------------+----------+
 * |   1.    |       1         |                 |                 |    1     | f(1) := 1
 * +---------+-----------------+-----------------+-----------------+----------+
 * |   2.    |       1         |       1         |                 |    2     | f(2) := 2
 * +---------+-----------------+-----------------+-----------------+----------+
 * |   3.    |       2         |       1         |       1         |    4     | f(3) = f(2) + f(1) + f(0) = 2 + 1 + 1 = 4
 * +---------+-----------------+-----------------+-----------------+----------+
 * |   4.    |       4         |       2         |       1         |    7     | f(4) = f(3) + f(2) + f(1) = 4 + 2 + 1 = 7
 * +---------+-----------------+-----------------+-----------------+----------+
 * |   5.    |       7         |       4         |       2         |    13    | f(5) = f(4) + f(3) + f(2) = 7 + 4 + 2 = 13
 * +---------+-----------------+-----------------+-----------------+----------+
 * |   6.    |       13        |       7         |       4         |    24    | usw.
 * +---------+-----------------+-----------------+-----------------+----------+
 * |   7.    |       24        |       13        |       7         |    44    |
 * +---------+-----------------+-----------------+-----------------+----------+
 * 
 * Analog zur Fibonacci-Folge:
 * 
 *     f(n) := 1                          , falls n = 0,1
 *          := 2                          , falls n = 2
 *          := f(n-1) + f(n-2) + f(n-3)   , sonst
 *          
 */
public class KaninchenA extends MiniJava {

	private static int f(int n) {
		return ( n <= 1 ) ? 1 : ( n == 2 ) ? 2 : f(n-1) + f(n-2) + f(n-3); 
	}
	
	public static void main(String[] args) {
		int n = readInt("Geben Sie eine postive ganze Zahl f�r Monat ein: ");
		while ( n < 1 ) {
			n = readInt("Geben Sie eine postive ganze Zahl f�r Monat ein: ");
		}
		write("Anzahl geschlechtsreifer Kaninchenpaare im " + n + ". Monat ist:\n" + f(n));
	}
	
}
