﻿Aufgabe 2.1 (P)

+==================================================================+
| Ganze Zahlen ohne führende Nullen mit Java SE 7 Underscores      |
+==================================================================+

Regel: 
http://docs.oracle.com/javase/7/docs/technotes/guides/language/underscores-literals.html

You can place underscores only between digits.
You cannot place underscores in the following places:
- At the beginning or end of a number
- Adjacent to a decimal point in a floating point literal
- Prior to an F or L suffix
- In positions where a string of digits is expected


Musterlösung:

PDigit  ::=  1 | ... | 9
Digit   ::=  0 | PDigit
Number  ::=  0 | -? PDigit ( _* Digit )*


Alternative Lösung 1:

PDigit  ::=  1 | ... | 9
Digit   ::=  0 | PDigit
Number  ::=  0 | -? PDigit (( _ | Digit )* Digit)?
        ::=  0 | -? PDigit ( _* Digit* Digit)?
        ::=  0 | -? PDigit ( _* Digit )*




==========================================================================================================================

Aufgabe 2.2 (P)

Operator: *, ?, |

Regulärer Ausdruck für + 
  a+  ::=  a a*
  a+  ::=  a* a

letter ::= a | ... | z

1. (a | c | ... | z)*
2. (a | b) letter* c
3. (a letter* b) | (b letter* a)




==========================================================================================================================

Aufgabe 2.3 (P)








==========================================================================================================================
Aufgabe 2.5 (H) Grammatik Gleitkommazahlen (Floating Point Numbers)

+==================================================================+
| Gleitkommazahlen ohne führende Nullen mit Java SE 7 Underscores  |
+==================================================================+

Regel: 
http://docs.oracle.com/javase/7/docs/technotes/guides/language/underscores-literals.html
https://docs.oracle.com/javase/tutorial/java/nutsandbolts/datatypes.html

You can place underscores only between digits.
You cannot place underscores in the following places:
- At the beginning or end of a number
- Adjacent to a decimal point in a floating point literal
- Prior to an F or L suffix
- In positions where a string of digits is expected

Beispiele :
Common Notation     : 12.345    , 9.81     , 340.25    , ...
Scientific Notation : 1.234E+1  , 981e-22  , 0.34025e312 , ...
                      0.1234e+2 , 98.1E-1  , 3.4025E2  , ...
                      12E34     , 1200.0   , 12.1e19   , ...
                      

- Exponent besteht aus GANZEN ZAHLEN.
- Jede Gleitkommazahl muss das Gleitkomma (Punkt, .) enthalten.

Zuerst Gleitkommzahlen MIT führenden Nullen (Vorlesung S. 46):
digit  ::=  0 | ... | 9
exp    ::=  (e | E) (+ | -)? digit digit*
float  ::=  (+ | - )?  digit digit* exp | digit* ( digit . | . digit ) digit* exp?


VALID : 2., 2.0, 1., 0., .0, .1, .3, 32E199, 45E0, ...
        1____0., .0___1, 2.E2
        3.14    10.    .001    1e100    3.14e-10    0e0

INVALID: 12.1e_19, 5_.22, 42_E3 , 45E0_, _45E0, 45E_0, ...
         012.0, ._0, ._1, 0_., 1_., 
         0___1., 

Gleitkommazahlen OHNE führende Nullen UND MIT Java EE 7 Underscore:
PDigit  ::=  1 | ... | 9                    /* (Positive) Ziffer 1..9 */
Digit   ::=  0 | PDigit
UDigit  ::=  ( Digit _* )* Digit            /* Ziffer 0..9 mit erlaubten Underscore(s) */
PInt    ::=  0 | PDigit ( _* Digit )*       /* Positive ganze Zahlen mit Underscore(s) */
Exp     ::=  (e | E) (+ | -)? PInt
Float   ::=  (+ | -)?   PInt Exp | ( PInt . | . UDigit ) UDigit* Exp?



Python Floating Point Literials
  https://docs.python.org/2.3/ref/floating.html



==========================================================================================================================
Aufgabe 2.6 (H)

letter ::= a | ... | z
L      ::= b | ... | z

1. a? (L a?)* 
   ==> Test : a, b, ..., z, bb, bbb, ..., zzz, ab, aba, abc, abac. abacd, abacaee, bca, bcad
   
2. letter letter | letter letter letter | letter letter letter letter

3. (L* a L* a L*)+
   ==> Test : aa, aaaa, ..., aba, abaaa, abcad, aabc, baa, baca, baabaca, bbaabacda, ...



==========================================================================================================================
Aufgabe 2.7 (H) Kaninchenpopulation

a)

/**
 *
 * Anfangs wird ein geschlechtsreifes Kaninchenpaar auf der Insel ausgesetzt.
 * Jedes Kaninchenpaar ist im ersten Monat geschlechtsreif.
 * Jedes Kaninchenpaar erzeugt jeden Monat ein neues Kaninchenpaar.
 * Jedes Kaninchen hat genau 3 Monate Lebenszeit. 
 *
 * +---------+-----------------+-----------------+-----------------+
 * |  Monat  |  1. Generation  |  2. Generation  |  3. Generation  |
 * +---------+-----------------+-----------------+-----------------+
 * |   1.    |       1         |                 |                 | Das erste Kaninchenpaar K1 ist anfangs schon schlechtsreif
 * |         |                 |                 |                 | K1 erzeugt ein neues Kaninchenpaar K2. (K2 ist noch nicht geschlechtsreif).
 * |         |                 |                 |                 | K2 ist die erste Erzeugung (Generation) von K1 ==> 1.Generation = 1
 * +---------+-----------------+-----------------+-----------------+
 * |   2.    |       1         |       1         |                 | Nach dem 1. Monat und während des 2. Monats ist K2 auch geschlechtsreif.
 * |         |                 |                 |                 | Nun gibt es 2 geschlechtsreife Paare, naemlich K1 und K2.
 * |         |                 |                 |                 | In diesem 2. Monat erzeugt K1 das Paar K3, und K2 das Paar K4.
 * |         |                 |                 |                 | (K3 und K4 sind noch nicht geschlechtsreif).
 * |         |                 |                 |                 | K3 ist die zweite Erzeugung (Generation) von K1 ==> 2. Generation = 1
 * |         |                 |                 |                 | K4 ist die erste Erzeugung (Generation) von K2 ==> 1. Generation = 1 
 * +---------+-----------------+-----------------+-----------------+
 * |   3.    |       2         |       1         |       1         | Nach dem 2. Monat und während des 3. Monats sind K3 und K4 geschlechtsreif.
 * |         |                 |                 |                 | Es sind jetzt 4 geschlechtsreife Kaninchenpaare K1, K2, K3, K4.
 * |         |                 |                 |                 | K1 erzeugt K5 ==> Dritte Erzeugung ==> 3. Generation = 1
 * |         |                 |                 |                 | K2 erzeugt K6 ==> Zweite Erzeugung ==> 2. Generation = 1
 * |         |                 |                 |                 | K3 erzeugt K7, K4 erzeugt K8, jeweils in erster Erzeugung ==> 1. Generation = 1 + 1 = 2
 * |         |                 |                 |                 | K5, K6, K7, K8 sind noch nich geschlechtsreif.
 * +---------+-----------------+-----------------+-----------------+
 * |   4.    |       4         |       2         |       1         | Nach dem 3. Monat stirbt K1. (Lebenszeit von 3 Monate vollendet).
 * |         |                 |                 |                 | K5, K6, K7, K8 sind nun geschlechtsreif.
 * |         |                 |                 |                 | Es sind insg. 7 geschlechtsreife Kaninchenpaare K2, K3, .., K8.
 * |         |                 |                 |                 | K5, K6, K7, K8 erzeugen jeweils in ihrer ersten Erzeugung (Generation) ein neues Paar.
 * |         |                 |                 |                 |   ==> 1. Generation = 1 + 1 + 1 + 1 = 4
 * |         |                 |                 |                 | K3, K4 erzeugen jeweils in ihrer zweiten Erzeugung (Generation) ein neues Paar.
 * |         |                 |                 |                 |   ==> 2. Generation = 1 + 1 = 2
 * |         |                 |                 |                 | K2 erzeugt in seiner dritten Erzeugung (Generation) ein neues Kaninchenpaar.
 * |         |                 |                 |                 |   ==> 3. Generation = 1
 * +---------+-----------------+-----------------+-----------------+
 * |   5.    |       7         |       4         |       2         |  usw.
 * +---------+-----------------+-----------------+-----------------+
 * |   6.    |       13        |       7         |       4         |
 * +---------+-----------------+-----------------+-----------------+
 * |   7.    |       24        |       13        |       7         |
 * +---------+-----------------+-----------------+-----------------+
 * 
 * Zusammenfassung :
 * 
 * +---------+-----------------+-----------------+-----------------+----------+
 * |  Monat  |  1. Generation  |  2. Generation  |  3. Generation  |  Gesamt  | f(0) := 1
 * +---------+-----------------+-----------------+-----------------+----------+
 * |   1.    |       1         |                 |                 |    1     | f(1) := 1
 * +---------+-----------------+-----------------+-----------------+----------+
 * |   2.    |       1         |       1         |                 |    2     | f(2) := 2
 * +---------+-----------------+-----------------+-----------------+----------+
 * |   3.    |       2         |       1         |       1         |    4     | f(3) = f(2) + f(1) + f(0) = 2 + 1 + 1 = 4
 * +---------+-----------------+-----------------+-----------------+----------+
 * |   4.    |       4         |       2         |       1         |    7     | f(4) = f(3) + f(2) + f(1) = 4 + 2 + 1 = 7
 * +---------+-----------------+-----------------+-----------------+----------+
 * |   5.    |       7         |       4         |       2         |    13    | f(5) = f(4) + f(3) + f(2) = 7 + 4 + 2 = 13
 * +---------+-----------------+-----------------+-----------------+----------+
 * |   6.    |       13        |       7         |       4         |    24    | usw.
 * +---------+-----------------+-----------------+-----------------+----------+
 * |   7.    |       24        |       13        |       7         |    44    |
 * +---------+-----------------+-----------------+-----------------+----------+
 * 
 * Analog zur Fibonacci-Folge:
 * 
 *     f(n) := 1                          , falls n = 0,1
 *          := 2                          , falls n = 2
 *          := f(n-1) + f(n-2) + f(n-3)   , sonst
 *          
 */
public class KaninchenA extends MiniJava {

	private static int f(int n) {
		return ( n <= 1 ) ? 1 : ( n == 2 ) ? 2 : f(n-1) + f(n-2) + f(n-3); 
	}
	
	public static void main(String[] args) {
		int n = readInt("Geben Sie eine postive ganze Zahl für Monat ein: ");
		while ( n < 1 ) {
			n = readInt("Geben Sie eine postive ganze Zahl für Monat ein: ");
		}
		write("Anzahl geschlechtsreifer Kaninchenpaare im " + n + ". Monat ist:\n" + f(n));
	}
	
}



b)

/**
 * Update:  (2. Generation) = 3 * (1. Generation)
 *
 * +---------+-----------------+-----------------+-----------------+-----------------------+
 * |  Monat  |  1. Generation  |  2. Generation  |  3. Generation  |  Anzahl reifer Paare  |
 * |         |         f       |        f        |       f         |        F(Monat)       | F(0) := 1
 * +---------+-----------------+-----------------+-----------------+-----------------------+
 * |   1.    |       1         |                 |                 |          1            | F(1) := 1
 * +---------+-----------------+-----------------+-----------------+-----------------------+
 * |   2.    |       1         |       1         |                 |          2            | F(2) := 2
 * +---------+-----------------+-----------------+-----------------+-----------------------+
 * |   3.    |       4         |       1         |       1         |          6            | F(3) = f(2) + 3*f(1) + f(0) = 2 + 3*1 + 1 = 6
 * +---------+-----------------+-----------------+-----------------+-----------------------+
 * |   4.    |       8         |       4         |       1         |          13           | F(4) = f(3) + 3*f(2) + f(1) = 6 + 3*2 + 1 = 13
 * +---------+-----------------+-----------------+-----------------+-----------------------+
 * |   5.    |       21        |       8         |       4         |          33           | F(5) = f(4) + 3*f(3) + f(2) = 13 + 3*6 + 2 = 33
 * +---------+-----------------+-----------------+-----------------+-----------------------+
 * |   6.    |       49        |       21        |       8         |          78           | F(6) = f(5) + 3*f(4) + f(3) = 33 + 3*13 + 6 = 78
 * +---------+-----------------+-----------------+-----------------+-----------------------+
 * 
 * 
 * <Unten stimmt nicht mehr>
 *
 * Anfangs wird ein geschlechtsreifes Kaninchenpaar auf der Insel ausgesetzt.
 * Jedes Kaninchenpaar ist im ersten Monat geschlechtsreif.
 * Jedes Kaninchen hat genau 3 Monate Lebenszeit.
 * 
 * Kaninchenpaare, die sich im zweiten Monat ihrer Geschlechtsreife (2. Generation) befinden, erzeugen jeweils 3 neue Kaninchenpaare.
 * 
 * In allen anderen Generationen (1. und 3.) erzeugt ein Kaninchenpaar weiterhin nur ein neues Kaninchenpaar.
 *
 * +---------+-----------------+-----------------+-----------------+
 * |  Monat  |  1. Generation  |  2. Generation  |  3. Generation  |
 * +---------+-----------------+-----------------+-----------------+
 * |   1.    |       1         |                 |                 | Das erste Kaninchenpaar K1 ist anfangs schon schlechtsreif
 * |         |                 |                 |                 | K1 erzeugt ein neues Kaninchenpaar K2. 
 * |         |                 |                 |                 | (K2 ist noch nicht geschlechtsreif).
 * |         |                 |                 |                 | K2 ist die erste Erzeugung (Generation) von K1 ==> 1.Generation = 1
 * +---------+-----------------+-----------------+-----------------+
 * |   2.    |       1         |       3         |                 | Nach dem 1. Monat und während des 2. Monats ist K2 auch geschlechtsreif.
 * |         |                 |                 |                 | Nun gibt es 2 geschlechtsreife Paare, naemlich K1 und K2.
 * |         |                 |                 |                 | In diesem 2. Monat erzeugt K1 in seiner 2. Erzeugung drei neue Paare K3, K4, K5.
 * |         |                 |                 |                 | K2 erzeugt in seiner ersten Erzeugung nur das Kaninchenpaar K6.
 * |         |                 |                 |                 | (K3, K4, K5 und K6 sind noch nicht geschlechtsreif).
 * |         |                 |                 |                 | K3, K4, K5 sind die zweite Erzeugung (Generation) von K1 ==> 2. Generation = 3
 * |         |                 |                 |                 | K6 ist die erste Erzeugung (Generation) von K2 ==> 1. Generation = 1 
 * +---------+-----------------+-----------------+-----------------+
 * |   3.    |       4         |       3         |       1         | Nach dem 2. Monat und während des 3. Monats sind K3, K4, K5, K6 geschlechtsreif.
 * |         |                 |                 |                 | Es sind jetzt 6 geschlechtsreife Kaninchenpaare: K1, K2, K3, K4, K5, K6.
 * |         |                 |                 |                 | K1 erzeugt K7 ==> Dritte Erzeugung ==> 3. Generation = 1
 * |         |                 |                 |                 | K2 erzeugt K8, K9, K10 ==> Zweite Erzeugung ==> 2. Generation = 3
 * |         |                 |                 |                 | K3, K4, K5, K6 erzeugen jeweils in erster Erzeugung ein neues Paar (K11,..., K14)
 * |         |                 |                 |                 |     ==> 1. Generation = 1 + 1 + 1 + 1 = 4
 * |         |                 |                 |                 | K7,..., K14 sind noch nich geschlechtsreif.
 * +---------+-----------------+-----------------+-----------------+
 * |   4.    |       8         |       12        |       1         | Nach dem 3. Monat stirbt K1. (Lebenszeit von 3 Monate vollendet).
 * |         |                 |                 |                 | K7,..., K14 sind jetzt geschlechtsreif.
 * |         |                 |                 |                 | Es sind insg. 13 geschlechtsreife Kaninchenpaare K2, K3, .., K14.
 * |         |                 |                 |                 | K2 erzeugt K15 ==> Dritte Erzeugung ==> 3. Generation = 1
 * |         |                 |                 |                 | K3, K4, K5, K6 erzeugen jeweils in ihrer zweiten Erzeugung 3 neue Kaninchenpaare:
 * |         |                 |                 |                 |     (K16,K17,K18), (K19,K20,K21), (K22,K23,K24), (K25,K26,K27)
 * |         |                 |                 |                 |     ==> 2. Generation = 3 + 3 + 3 + 3 = 4*3 = 12
 * |         |                 |                 |                 | K7,..., K14 erzeugen jeweils in ihrer ersten Erzeugung (Generation) ein neues Paar:
 * |         |                 |                 |                 |     K28, ..., K35
 * |         |                 |                 |                 |     ==> 1. Generation = 1 + ... + 1 = 8*1 = 8
 * |         |                 |                 |                 | K15,..., K35 sind noch nicht geschlechtsreif.
 * +---------+-----------------+-----------------+-----------------+
 * |   5.    |       21        |       24        |       4         | Nach dem 4. Monat stirbt K2. (Lebenszeit von 3 Monate vollendet).
 * |         |                 |                 |                 | K15,..., K35 sind jetzt geschlechtsreif.
 * |         |                 |                 |                 | Es sind insg. 33 geschlechtsreife Kaninchenpaare K3, .., K35.
 * |         |                 |                 |                 | K3, K4, K5, K6 erzeugen jeweils in ihrer dritten Erzeugung (Generation) ein neues Paar:
 * |         |                 |                 |                 |     K36, ..., K39
 * |         |                 |                 |                 |     ==> 3. Generation = 1 + 1 + 1 + 1 = 4
 * |         |                 |                 |                 | K7,..., K14 erzeugen in ihrer zweiten Erzeugung (Generation) jeweils 3 neue Paare:
 * |         |                 |                 |                 |     (K40,K41,K42), ...., (K61,K62,K63)
 * |         |                 |                 |                 |     ==> 2. Generation = 8*3 = 24
 * |         |                 |                 |                 | K15,..., K35 erzeugen in ihrer ersten Erzeugung (Generation) jeweils ein neues Paar: 
 * |         |                 |                 |                 |     K64, ..., K84
 * |         |                 |                 |                 |     ==> 1. Generation = 21 * 1 = 21
 * |         |                 |                 |                 | K36,..., K84 sind noch nicht geschlechtsreif.
 * +---------+-----------------+-----------------+-----------------+
 * |   6.    |       49        |       63        |        8        | Nach dem 5. Monat sterben K3, K4, K5, K6. (Lebenszeit von 3 Monate vollendet)
 * |         |                 |                 |                 | K36,..., K84 sind nun geschlechtsreif.
 * |         |                 |                 |                 | Es sind insg. 78 geschlechtsreife Kaninchenpaare K7, ..., K84.
 * |         |                 |                 |                 | K7,..., K14 erzeugen in ihrer dritten Erzeugung (Generation) jeweils ein neues Paar:
 * |         |                 |                 |                 |     K85, ..., K92
 * |         |                 |                 |                 |     ==> 3. Generation = 8*1 = 8
 * |         |                 |                 |                 | K15,..., K35 erzeugen in ihrer zweiten Erzeugung (Generation) jeweils 3 neue Paare: 
 * |         |                 |                 |                 |     (K93,K94,K95), ..., (K153,K154,K155)
 * |         |                 |                 |                 |     ==> 2. Generation = 21 * 3 = 63
 * |         |                 |                 |                 | K36,..., K84 erzeugen in ihrer ersten Erzeugung (Generation) jeweils ein neues Paar:
 * |         |                 |                 |                 |     K156, ..., K204
 * |         |                 |                 |                 |     ==> 1. Generation = 49*1 = 49
 * |         |                 |                 |                 | K85,..., K204 sind noch nicht geschlechtsreif. 
 * +---------+-----------------+-----------------+-----------------+
 * 
 * Zusammenfassung : 
 *   Wir sehen, dass die Anzahl der 2. Generation im k-ten Monat dem 3fachen der Anzahl 
 *   der 1. Generation im (k-1)ten Monat entspricht.
 *   Das laesst sich dadurch erklaeren, dass jedes reife Kaninchenpaar in seinem 2ten Monat
 *   immer 3 neue Paare zur Welt bringt --> Das verdreifacht die Anzahl im letzten (ersten)
 *   Monat einfach.
 * 
 * 
 * Definiere:
 *   n : Monat, 
 *   j : Generation, wobei j = 1,2,3
 *   
 *   g(n,j) : Anzahl reifer Paare im n-ten Monat von j-ten Generation
 *   
 *   f(n) := 1                            , fuer n = 1,2
 *        := 4                            , fuer n = 3 
 *        := f(n-1) + 3*f(n-2) + f(n-3)   , sonst (n > 3)
 *          
 *                   
 *  Definiere die Funktionen:
 *    1)  Anzahl reifer Paare im n-ten Monat der 1. Generation:
 *    
 *            g(n,1) := f(n)
 *                   
 *    2)  Anzahl reifer Paare im n-ten Monat der 2. Generation:
 *    
 *            g(n,2) := 0                            , fuer n = 1
 *                   := 3 * g(n-1, 1)                , sonst ( n > 1 )
 *    
 *    3)  Anzahl reifer Paare im n-ten Monat der 3. Generation:
 *    
 *            g(n,3) := 0                            , fuer n = 1,2
 *					 := g(n-2,1)                     , sonst ( n > 2 )
 *
 *    4)  Anzahl aller reifen Paare im n-ten Monat:
 *    
 *            F(n) := 1                            , fuer n = 0,1
 *				   := 2                            , fuer n = 2
 *                 := F(n-1) + 3*F(n-2) + F(n-3)   , sonst ( n > 2 )
 *                   
 * +---------+-----------------+-----------------+-----------------+-----------------------+
 * |  Monat  |  1. Generation  |  2. Generation  |  3. Generation  |  Anzahl reifer Paare  |
 * |         |                 |                 |                 |        F(Monat)       | F(0) := 1
 * +---------+-----------------+-----------------+-----------------+-----------------------+
 * |   1.    |       1         |                 |                 |          1            | F(1) := 1
 * +---------+-----------------+-----------------+-----------------+-----------------------+
 * |   2.    |       1         |       3         |                 |          2            | F(2) := 2
 * +---------+-----------------+-----------------+-----------------+-----------------------+
 * |   3.    |       4         |       3         |       1         |          6            | F(3) = f(2) + 3*f(1) + f(0) = 2 + 3*1 + 1 = 6
 * +---------+-----------------+-----------------+-----------------+-----------------------+
 * |   4.    |       8         |       12        |       1         |          13           | F(4) = f(3) + 3*f(2) + f(1) = 6 + 3*2 + 1 = 13
 * +---------+-----------------+-----------------+-----------------+-----------------------+
 * |   5.    |       21        |       24        |       4         |          33           | F(5) = f(4) + 3*f(3) + f(2) = 13 + 3*6 + 2 = 33
 * +---------+-----------------+-----------------+-----------------+-----------------------+
 * |   6.    |       49        |       63        |       8         |          78           | F(6) = f(5) + 3*f(4) + f(3) = 33 + 3*13 + 6 = 78
 * +---------+-----------------+-----------------+-----------------+-----------------------+
 *          
 */
public class KaninchenB extends MiniJava {

	/**  
	 *  Hilfsfunktion:
	 *  
	 *    f(n) := 1                            , fuer n = 1,2
	 *         := 4                            , fuer n = 3 
	 *         := f(n-1) + 3*f(n-2) + f(n-3)   , sonst ( n > 3 )
	 */
	private static int f (int n) {
		return ( n < 3 ) ? 1 : ( n == 3 ) ? 4 : f(n-1) + 3*f(n-2) + f(n-3);
	}
	
	/**
	 *    1)  Anzahl reifer Paare im n-ten Monat der 1. Generation:
     *    
     *            g(n,1) := f(n)
     *                   
     *    2)  Anzahl reifer Paare im n-ten Monat der 2. Generation:
     *    
     *            g(n,2) := 0                            , fuer n = 1
     *                   := 3 * g(n-1, 1)                , sonst ( n > 1 )
     *    
     *    3)  Anzahl reifer Paare im n-ten Monat der 3. Generation:
     *    
     *            g(n,3) := 0                            , fuer n = 1,2
     *					 := g(n-2,1)                     , sonst ( n > 2 )
     *
	 * @param n : Monat
	 * @param j : Generation (1,2,3)
	 * @return Anzahl geschlechtsreifer Kaninchenpaare im n-ten Monat in der j-ten Generation.
	 */
	private static int g (int n, int j) {
		return ( j == 1 ) ? f(n) : ( j == 2 ) ? ( ( n == 1 ) ? 0 : 3 * g(n-1, 1) ) : ( n == 1 || n == 2 ) ? 0 : g(n-2, 1);
	}
	
	public static void main(String[] args) {
		int n = readInt("Geben Sie eine postive ganze Zahl für Monat ein: ");
		while ( n < 1 ) {
			n = readInt("Geben Sie eine postive ganze Zahl für Monat ein: ");
		}
		String s = "Monat : " + n + "\n"
				 + "1. Generation : " + g(n,1) + "\n"
				 + "2. Generation : " + g(n,2) + "\n"
				 + "3. Generation : " + g(n,3);
		write(s);
	}
	
}


