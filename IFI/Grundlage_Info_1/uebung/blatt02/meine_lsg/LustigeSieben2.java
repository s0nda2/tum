
public class LustigeSieben2 extends MiniJava {

	public static void main(String[] args) {
		int guthaben, einsatz, gewinn, feld;
		int summe; // Augensumme
		
		guthaben = 100;
		einsatz = 1;
		gewinn = 0;
		
		write("Lasst uns spielen. Du hast 100 Chips zur Verf�gung");
		while ( guthaben > 0 && einsatz > 0 ) {
			einsatz = readInt("Wie viel Chips willst Du setzen ?");
			if ( einsatz != 0 ) {
				while ( einsatz < 0 ) {
					einsatz = readInt("Ungueltige Eingabe. Dein Guthaben betraegt " + guthaben + ". Was ist Dein Einsatz ?");
				}
				while ( einsatz > guthaben ) {
					einsatz = readInt("Soviel Chips hast Du nicht mehr. Dein Guthaben betraegt " + guthaben + ". Was ist Dein Einsatz ?"); 
				}
				
				guthaben -= einsatz;
				
				feld = readInt("Auf welches Feld willst Du setzen ?");
				while ( feld < 2 || feld > 12 ) {
					feld = readInt("Es gibt nur die Felder 2 bis 12. Auf welches Feld willst Du setzen ?");
				}
				write("Rien ne va plus. Du hast " + einsatz + " Chips auf die " + feld + " gesetzt.");
				
				summe = dice() + dice();
				write("Die W�rfel sind gefallen: " +  summe + " gewinnt.");
				
				if ( summe == feld ) {
					gewinn = ( feld == 7 ) ? 3*einsatz : 2*einsatz;
				} else if ( (summe >= 2 && summe <= 6) &&  (feld >= 2 && feld <= 6) || 
						    (summe >= 8 && summe <= 12) && (feld >= 8 && feld <= 12) ) {
						gewinn = einsatz; } 
				else {
						gewinn = 0;
				}				
				
				guthaben += gewinn;
				
				write("Du bekommst " + gewinn + " Chips. Dein Guthaben betraegt nun: " + guthaben + " Chips.");
			}
		}
		
		if ( guthaben == 0 ) {
			write("Du hast leider alles verloren.");
		} else {
			write("Du gewinnst mit " + guthaben + " Chips. Herzlichen Glueckwunsch.");
		}
		
	}
}
