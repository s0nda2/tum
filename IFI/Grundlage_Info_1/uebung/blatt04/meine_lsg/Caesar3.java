
public class Caesar3 extends MiniJava {

	/*
	 * ASCII
	 * 'A' - 'Z' : 65 - 90
	 * 'a' - 'z' : 97 - 122
	 */
	public static void main(String[] args) {
		/*
		 * Deutscher Alphabet hat 26 Buchstaben.
		 * Nur Buchstaben 'a' bis 'z' sowie 'A' bis 'Z' werden beruecksichtigt
		 * und ver/entschluesselt. Alle anderen Zeichen werden nicht
		 * verschluesselt.
		 */
		String input = readString(), chiffre = "";
		int key = read(), i = 0;
		
		key %= 26;		
		
		while ( i < input.length() ) {
			char c = input.charAt(i);
			if ( c >= 'A' && c <= 'Z' ) {
				chiffre += (char) ('A' + (((c + (26+key)) - 'A') % 26));
			}
			else if ( c >= 'a' && c <= 'z' )  {
				chiffre += (char) ('a' + (((c + (26+key)) - 'a') % 26)); 
			} else {
				chiffre += c;
			}
			++i;
		}
			
		write(chiffre);
	}
}
