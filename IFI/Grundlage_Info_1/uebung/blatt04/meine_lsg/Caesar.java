
public class Caesar extends MiniJava {

	public static void main(String[] args) {
		/* Der Einfachheit halber werden nur Buchstaben a bis z und A bis Z verschluesselt.
		 * D.h. alle anderen Zeichen sollen nicht verschluesselt werden und tauchen somit
		 * als Klartext im Geheimtext auf.
		 * 
		 * a..z ( A..Z ) : 26 Zeichen
		 */
		String input, output;
		int key, i;
		
		input = readString(); // System.out.println("Input : " + input);
		key = read(); // System.out.println("Key : " + key);
		key %= 26;
		if ( key < 0 ) { key += 26; }
		
		output = "";
		i = 0;
		while ( i < input.length() ) {
			char ch = input.charAt(i);
			int diff = 0;
			if ( ch >= 'a' && ch <= 'z' ) { // Kleinbuchstabe
				diff = 'z' - ch;
				output += ( diff < key ) ? (char) ('a' + (key - diff - 1)) : (char) (ch + key);  
			} else if ( ch >= 'A' && ch <= 'Z' ) { // Grossbuchstabe
				diff = 'Z' - ch;
				output += ( diff < key ) ? (char) ('A' + (key - diff - 1)) : (char) (ch + key);
			} else {
				output += ch;
			}
			++i;
		}
		
		write(output); // System.out.println("Output : " + output);
	}
}
