
public class Verhext extends MiniJava {

	// x^y
	public static int pow(int x, int y) {
		return java.math.BigInteger.valueOf(x).pow(y).intValueExact();
	}
			
	public static void main(String[] args) {
		String input = readString();
		int output;
		
		/* input your solution here */
		boolean valid;
		int ms /* Number of most significant bit */ , 
			uScores /* Number of underscores in string */,
			endIndex /* End index */; 
		int hex[] = {10, /* A */
					 11, /* B */
					 12, /* C */
					 13, /* D */
					 14, /* E */
					 15  /* F */ };
		
		/*
		 * Wir gehen davon aus, dass der Eingabestring eine gueltige Zahl in
		 * hexadezimaler Form ist. Und dass der String eine Zahl beschreibt, 
		 * die in einen Integer passt, d.h. Suffix l oder L kommt nicht vor.
		 */
		
		ms = input.length() - 1;
		output = uScores = 0;
		valid = false;
		
		/* Catch invalid inputs */
		while ( !valid ) {
			valid = true;
			int leadingZero = -1;
			if ( input.charAt(0) == '-' ) { // negative hex
				leadingZero = 1;
				if ( input.charAt(1) != '0' || ( input.charAt(2) != 'x' && input.charAt(2) != 'X' ) ) 
					valid = false;
				if ( input.charAt(3) == '_' )
					valid = false;
			} else { // positive hex
				leadingZero = 0;
				if ( input.charAt(0) != '0' || ( input.charAt(1) != 'x' && input.charAt(1) != 'X' ) ) 
					valid = false;
				if ( input.charAt(2) == '_' )
					valid = false;
			}
			if ( valid && leadingZero != -1 ) {
				int j = leadingZero + 2;
				if ( !(input.charAt(j) >= 'a' && input.charAt(j) <= 'f' ||
					   input.charAt(j) >= 'A' && input.charAt(j) <= 'F' || 
					   input.charAt(j) >= '0' && input.charAt(j) <= '9' ) ||
					 !(input.charAt(ms) >= 'a' && input.charAt(ms) <= 'f' ||
					   input.charAt(ms) >= 'A' && input.charAt(ms) <= 'F' || 
					   input.charAt(ms) >= '0' && input.charAt(ms) <= '9' ) ) {
					valid = false;
				} else {
					for ( j = leadingZero + 3; j < ms-1; j++ ) {
						if ( !(input.charAt(j) >= 'a' && input.charAt(j) <= 'f' ||
								input.charAt(j) >= 'A' && input.charAt(j) <= 'F' || 
								input.charAt(j) >= '0' && input.charAt(j) <= '9' ||
								input.charAt(j) == '_') ) {
							valid = false;
							break;
						}
					}
				}
			}
			if ( !valid ) {
				input = readString("<html>Die letzte Eingabe <font color=\"red\">"
						+ input + "</font> war fehlerhaft.<br />Neue Eingabe :");
			}
		}
		
		ms = input.length() - 1; // Update for new input
		endIndex = ( input.charAt(0) != '-' ) ? 2 : 3; // Handle negative sign (-)
		
		/* Calculate integer value from hexa-decimal string */
		for ( int i = ms; i >= endIndex; i-- ) {
			char ch = input.charAt(i);
			if ( ch == '_' )
				uScores = uScores + 1;
			else {
				if ( ch >= 'A' && ch <= 'F' || ch >= 'a'  && ch <= 'f' ) {
					if ( ch >= 'a' && ch <= 'f' ) {
						ch -= 32; // 32 = 97 - 65 = 'a' - 'A' 
					}
					output += hex[(int) ch - 65] * pow(16, ms-i-uScores);
				}
				else { // Character '0' (48) until '9' (57)
					output += ((int) ch - 48) * pow(16, ms-i-uScores);  
				}
			}
		}
		
		hex = null;
		
		if ( input.charAt(0) == '-' )
			write("-" + output);
		else
			write(output);
	}
}
