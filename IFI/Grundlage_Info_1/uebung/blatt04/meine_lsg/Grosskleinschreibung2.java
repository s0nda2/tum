
public class Grosskleinschreibung2 extends MiniJava {

	public static void main(String[] args) {
		/* ASCII
		 * 		[A..Z] : 65-90
		 * 		[a..z] : 97-122
		 */
		String input = readString("Geben Sie einen String ein!");
		String output = "";
				
		int i = 0;
		while ( i < input.length() ) {
			char c = input.charAt(i);
			output += ( c >= 'A' && c <= 'Z' ) ? /* Grossbuchstabe */ (char) (c+32) /* 32 = 98 - 65 = 'a' - 'A' */
					: ( c >= 'a' && c <= 'z' ) ? /* Kleinbuchstabe */ (char) (c-32)
					: c;
			i++;
		}
		write(output);
	}
}
