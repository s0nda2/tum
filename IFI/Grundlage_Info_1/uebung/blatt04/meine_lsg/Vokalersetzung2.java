
public class Vokalersetzung2 extends MiniJava {

    public static void main(String[] args) {
        String text = "Hat der alte Hexenmeister\n" +
                        "sich doch einmal wegbegeben!\n" +
                        "Und nun sollen seine Geister\n" +
                        "auch nach meinem Willen leben.\n" +
                        "Seine Wort und Werke\n" +
                        "merkt ich und den Brauch,\n" +
                        "und mit Geistesst�rke\n" +
                        "tu ich Wunder auch.\n" +
                        "Walle! walle\n" +
                        "Manche Strecke,\n" +
                        "da�, zum Zwecke,\n" +
                        "Wasser flie�e\n" +
                        "und mit reichem, vollem Schwalle\n" +
                        "zu dem Bade sich ergie�e.";
        
        /* Alle 5 Vokale : 
    	 * 		A (65), E (69),  I (73),  O (79),  U (85)
    	 * 		a (97), e (101), i (105), o (111), u (117)
    	 */
        
        String out;
        char vokal;
        
        /* Eingabe des gewuenschten Vokals */
        do { // Pruefe die Eingabe auf Richtigkeit
        	out = readString("Geben Sie einen Vokal :");
        	while ( out.length() > 1 ) { out = readString("Geben Sie einen Vokal :"); }
            vokal = out.charAt(0);
        } while ( !(vokal == 'A' || vokal == 'E' || vokal == 'I' || vokal == 'O' || vokal == 'U' ||
        			vokal == 'a' || vokal == 'e' || vokal == 'i' || vokal == 'o' || vokal == 'u' ) );
        // Mache Vokal zum Kleinbuchstaben
        if ( vokal < 97 ) { vokal += 32; } // 32 = 'a' - 'A' = 97 - 65
        
        /* Vokalersetzung */
        out = "";
        for ( int i = 0; i < text.length(); i++ ) {
        	char c = text.charAt(i);
        	// Falls c ein Vokal ist
        	if ( c == 'A' || c == 'E' || c == 'I' || c == 'O' || c == 'U' ||
      	  	  	 c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u' ) {
        		out += ( c >= 65 && c <= 85 ) ? /* c ist Grossbuchstabe */ (char) (vokal - 32)
        									  : /* c ist Kleinbuchstabe */ (char) vokal;
        	} else {
        		out += (char) c;
        	}        	
        }
        
        write(out);
    }

}
